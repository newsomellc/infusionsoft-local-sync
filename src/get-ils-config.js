/**
 * Gets the ILS config as an nconf object.
 */
'use strict';
const file_exists = require('fs').existsSync;
const pjoin       = require('path').join;
const NConf       = require('nconf').Provider;
const debug       = require('debug')('ils:conf:getconfig');
const chalk       = require('chalk');

module.exports = p =>
{
	//firstly, get the paths to our (possible) config files.
	let ilsfile, env, cwd, sysdir, homedir;

	if (typeof p === 'string')
	{
		ilsfile = p;
		p  = Object.create(null);
	}
	if (!p)
		p = Object.create(null);

	ilsfile = p.ilsfile !== false && p.ilsfile || ilsfile;
	cwd     = p.cwd     !== false && p.cwd     || process.cwd();
	env     = p.env     !== false && p.env     || process.env.NODE_ENV||'development';
	sysdir  = p.sysdir  !== false && p.sysdir  || (/^win/.test(process.platform) ? '/' : '/etc');
	homedir = p.homedir !== false && p.homedir || process.env.HOME || process.env.USERPROFILE;

	let conf = new NConf();
	conf.argv();
	conf.env();

	let project_env_file  = cwd     && pjoin(cwd, `infusionsoft-local-sync.${env}.json`);
	let project_base_file = cwd     && pjoin(cwd, 'infusionsoft-local-sync.json');
	let user_file         = homedir && pjoin(homedir, '.infusionsoft-local-sync.json');
	let system_file       = sysdir  && pjoin(sysdir, 'infusionsoft-local-sync.json');

	//Then, upon checking if they exist, add them as config options.
	if (ilsfile)
	{
		debug('trying to load config file:', ilsfile);
		if (ilsfile && ilsfile && file_exists(ilsfile))
			conf.file('ilsfile', ilsfile);
		else
			throw Error('Given config file doesn\'t exist');
	}

	if (project_env_file)
	{
		debug('trying to load config file:', project_env_file);
		if (file_exists(project_env_file))
			conf.file('project-env', project_env_file);
		else
			debug(project_env_file, 'doesn\'t exist');
	}

	if (project_base_file)
	{
		debug('trying to load config file:', project_base_file);
		if (file_exists(project_base_file))
			conf.file('project-base', project_base_file);
		else
			debug(project_base_file, 'doesn\'t exist');
	}

	if (user_file)
	{
		debug('trying to load config file:', user_file);
		if (file_exists(user_file))
			conf.file('user', user_file);
		else
			debug(user_file, 'doesn\'t exist');
	}

	if (system_file)
	{
		debug('trying to load config file:', system_file);
		if (file_exists(system_file))
			conf.file('system', system_file);
		else
			debug(system_file, 'doesn\'t exist');
	}

	if (!conf.get('api:app'))
		throw Error(chalk.cyan(`ILS is not configured or no config loaded. Type ${chalk.yellow('ils setup')} to get started.`));

	return conf;
}
