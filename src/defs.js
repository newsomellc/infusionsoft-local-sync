/**
 * Definitions.  Basically a programmatically readable version of Infusionsofts
 * table reference at:
 * https://developer.infusionsoft.com/docs/table-schema/
 * With idiotic stuff removed (RecurringOrderWithContact? WTF man, WTF)
 * They aresthat =>accepts =>-  I plan to use this not only for
 * setting up querying etc, but for building the DB migrations.
 *
 *
 * HOW TO USE:
 * Iterate over the exported module, and pass it an tsetup object.
 * It's up to it to implement the methods that define what this does (i.e. 
 * anything from setting up a GUI to setting up the database).
 * The plan is to centralize and DRY-ize this. Dependency injection is so neat.
 */
'use strict';
const debug  = require('debug')('infusionsoft-local-sync:defs');

module.exports =
{
	ActionSequence : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('TemplateName');
		tsetup.addStringField('VisibleToTheseUsers');
	},
	Affiliate : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('AffCode');
		tsetup.addStringField('AffName');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addIntegerField('DefCommissionType');
		tsetup.addFloatField('LeadAmt');
		tsetup.addIntegerField('LeadCookieFor');
		tsetup.addFloatField('LeadPercent');
		tsetup.addIntegerField('NotifyLead');
		tsetup.addIntegerField('NotifySale');
		tsetup.addFKField('ParentId', 'Affiliate');
		tsetup.addStringField('Password');
		tsetup.addIntegerField('PayoutType');
		tsetup.addFloatField('SaleAmt');
		tsetup.addFloatField('SalePercent');
		tsetup.addIntegerField('Status');
	},
	AffResource : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('Notes');
		tsetup.addStringField('ProgramIds');
		tsetup.addStringField('ResourceHREF');
		tsetup.addStringField('ResourceHTML');
		tsetup.addStringField('ResourceOrder');
		tsetup.addStringField('ResourceType');
		tsetup.addStringField('Title');
	},
	Campaign : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('Name');
		tsetup.addIntegerField('Status');
	},
	Campaignee : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('Campaign');
		tsetup.addFKField('CampaignId', 'Campaign');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addIntegerField('Status');
	},
	CampaignStep : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('CampaignId', 'Campaign');
		tsetup.addStringField('StepStatus');
		tsetup.addStringField('StepTitle');
		tsetup.addFKField('TemplateId', 'Template');
	},
	CCharge : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('Amt');
		tsetup.addStringField('ApprCode');
		tsetup.addStringField('CCId');
		tsetup.addStringField('MerchantId');
		tsetup.addIntegerField('OrderNum');
		tsetup.addFKField('PaymentId', 'Payment');
		tsetup.addStringField('RefNum');
	},
	Company : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('AccountId', 'Account');
		tsetup.addStringField('Address1Type');
		tsetup.addStringField('Address2Street1');
		tsetup.addStringField('Address2Street2');
		tsetup.addStringField('Address2Type');
		tsetup.addStringField('Address3Street1');
		tsetup.addStringField('Address3Street2');
		tsetup.addStringField('Address3Type');
		tsetup.addStringField('Anniversary');
		tsetup.addStringField('AssistantName');
		tsetup.addStringField('AssistantPhone');
		tsetup.addStringField('BillingInformation');
		tsetup.addStringField('Birthday');
		tsetup.addStringField('City');
		tsetup.addStringField('City2');
		tsetup.addStringField('City3');
		tsetup.addStringField('Company');
		tsetup.addStringField('CompanyID');
		tsetup.addStringField('ContactNotes');
		tsetup.addStringField('ContactType');
		tsetup.addStringField('Country');
		tsetup.addStringField('Country2');
		tsetup.addStringField('Country3');
		tsetup.addStringField('CreatedBy');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('Email');
		tsetup.addStringField('EmailAddress2');
		tsetup.addStringField('EmailAddress3');
		tsetup.addStringField('Fax1');
		tsetup.addStringField('Fax1Type');
		tsetup.addStringField('Fax2');
		tsetup.addStringField('Fax2Type');
		tsetup.addStringField('FirstName');
		tsetup.addStringField('Groups');
		tsetup.addStringField('JobTitle');
		tsetup.addStringField('LastName');
		tsetup.addStringField('LastUpdated');
		tsetup.addStringField('LastUpdatedBy');
		tsetup.addStringField('MiddleName');
		tsetup.addStringField('Nickname');
		tsetup.addStringField('OwnerID');
		tsetup.addStringField('Password');
		tsetup.addStringField('Phone1');
		tsetup.addStringField('Phone1Ext');
		tsetup.addStringField('Phone1Type');
		tsetup.addStringField('Phone2');
		tsetup.addStringField('Phone2Ext');
		tsetup.addStringField('Phone2Type');
		tsetup.addStringField('Phone3');
		tsetup.addStringField('Phone3Ext');
		tsetup.addStringField('Phone3Type');
		tsetup.addStringField('Phone4');
		tsetup.addStringField('Phone4Ext');
		tsetup.addStringField('Phone4Type');
		tsetup.addStringField('Phone5');
		tsetup.addStringField('Phone5Ext');
		tsetup.addStringField('Phone5Type');
		tsetup.addStringField('PostalCode');
		tsetup.addStringField('PostalCode2');
		tsetup.addStringField('PostalCode3');
		tsetup.addStringField('ReferralCode');
		tsetup.addStringField('SpouseName');
		tsetup.addStringField('State');
		tsetup.addStringField('State2');
		tsetup.addStringField('State3');
		tsetup.addStringField('StreetAddress1');
		tsetup.addStringField('StreetAddress2');
		tsetup.addStringField('Suffix');
		tsetup.addStringField('Title');
		tsetup.addStringField('Username');
		tsetup.addStringField('Validated');
		tsetup.addStringField('Website');
		tsetup.addStringField('ZipFour1');
		tsetup.addStringField('ZipFour2');
		tsetup.addStringField('ZipFour3');
		tsetup.dataFormId(-4);
	},
	Contact : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addDateField('DateCreated');
		tsetup.addDateField('LastUpdated');
		tsetup.addStringField('FirstName');
		tsetup.addStringField('MiddleName');
		tsetup.addStringField('LastName');
		tsetup.addStringField('Company');
		tsetup.addFKField('CompanyID', 'Company');
		tsetup.addStringField('ContactType');
		tsetup.addStringField('CreatedBy');
		tsetup.addFKField('OwnerID', 'User');

		tsetup.addStringField('Email');
		tsetup.addStringField('EmailAddress2');
		tsetup.addStringField('EmailAddress3');
		
		tsetup.addStringField('Phone1');
		tsetup.addStringField('Phone1Ext');
		tsetup.addStringField('Phone1Type');
		tsetup.addStringField('Phone2');
		tsetup.addStringField('Phone2Ext');
		tsetup.addStringField('Phone2Type');
		tsetup.addStringField('Phone3');
		tsetup.addStringField('Phone3Ext');
		tsetup.addStringField('Phone3Type');
		tsetup.addStringField('Phone4');
		tsetup.addStringField('Phone4Ext');
		tsetup.addStringField('Phone4Type');
		tsetup.addStringField('Phone5');
		tsetup.addStringField('Phone5Ext');
		tsetup.addStringField('Phone5Type');

		tsetup.addStringField('StreetAddress1');
		tsetup.addStringField('StreetAddress2');
		tsetup.addStringField('City');
		tsetup.addStringField('State');
		tsetup.addStringField('PostalCode');
		tsetup.addStringField('ZipFour1');
		tsetup.addStringField('Address1Type');
		tsetup.addStringField('Country');

		tsetup.addStringField('Address2Street1');
		tsetup.addStringField('Address2Street2');
		tsetup.addStringField('City2');
		tsetup.addStringField('State2');
		tsetup.addStringField('PostalCode2');
		tsetup.addStringField('ZipFour2');
		tsetup.addStringField('Address2Type');
		tsetup.addStringField('Country2');

		tsetup.addStringField('Address3Street1');
		tsetup.addStringField('Address3Street2');
		tsetup.addStringField('City3');
		tsetup.addStringField('State3');
		tsetup.addStringField('PostalCode3');
		tsetup.addStringField('ZipFour3');
		tsetup.addStringField('Address3Type');
		tsetup.addStringField('Country3');

		tsetup.addStringField('AccountId');
		tsetup.addDateField('Anniversary');
		tsetup.addStringField('AssistantName');
		tsetup.addStringField('AssistantPhone');
		tsetup.addStringField('BillingInformation');
		tsetup.addDateField('Birthday');
		tsetup.addStringField('ContactNotes');
		tsetup.addStringField('Fax1');
		tsetup.addStringField('Fax1Type');
		tsetup.addStringField('Fax2');
		tsetup.addStringField('Fax2Type');
		tsetup.addStringField('Groups');
		tsetup.addStringField('JobTitle');
		tsetup.addFKField('LastUpdatedBy', 'User');
		tsetup.addFKField('LeadSourceId', 'LeadSource');
		tsetup.addStringField('Leadsource');
		tsetup.addStringField('Nickname');
		tsetup.addStringField('Password');
		tsetup.addStringField('ReferralCode');
		tsetup.addStringField('SpouseName');
		tsetup.addStringField('Suffix');
		tsetup.addStringField('Title');
		tsetup.addStringField('Username');
		tsetup.addStringField('Validated');
		tsetup.addStringField('Website');
		tsetup.dataFormId(-1);
		tsetup.addTags();
		tsetup.alias('c');
	},
	ContactAction : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('Accepted');
		tsetup.addDateField('ActionDate');
		tsetup.addStringField('ActionDescription');
		tsetup.addStringField('ActionType');
		tsetup.addStringField('CompletionDate');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('CreatedBy');
		tsetup.addDateField('CreationDate');
		tsetup.addStringField('CreationNotes');
		tsetup.addDateField('EndDate');
		tsetup.addIntegerField('IsAppointment');
		tsetup.addDateField('LastUpdated');
		tsetup.addFKField('LastUpdatedBy', 'User');
		tsetup.addEnumField('ObjectType', ['Note', 'Task', 'Appointment']);
		tsetup.addFKField('OpportunityId', 'Lead');
		tsetup.addDateField('PopupDate');
		tsetup.addIntegerField('Priority');
		tsetup.addFKField('UserID', 'User');
		tsetup.dataFormId(-5);
		tsetup.alias('note', 'task', 'appointment', 'n');

	},
	ContactGroup : tsetup => 
	{
		tsetup.frontEndName('Tag');
		tsetup.addPKField('Id');
		tsetup.addFKField('GroupCategoryId', 'ContactGroupCategory');
		tsetup.addStringField('GroupDescription');
		tsetup.addStringField('GroupName');
		tsetup.alias('tag', 'tags', 't');

	},
	ContactGroupAssign : tsetup => 
	{
		tsetup.frontEndName('TagAssignment');
		tsetup.addStringField('ContactGroup');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addDateField('DateCreated');
		tsetup.addFKField('GroupId', 'ContactGroup');
		tsetup.unique(['ContactGroup', 'ContactId']);
		tsetup.alias('tagged', 'tagassign', 'tagassignment', 'tagassignment', 'ta');

	},
	ContactGroupCategory : tsetup => 
	{
		tsetup.frontEndName('TagCategory');
		tsetup.addPKField('Id');
		tsetup.addStringField('CategoryDescription');
		tsetup.addStringField('CategoryName');
	},
	CProgram : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addBooleanField('Active');
		tsetup.addStringField('BillingType');
		tsetup.addStringField('DefaultCycle');
		tsetup.addIntegerField('DefaultFrequency');
		tsetup.addFloatField('DefaultPrice');
		tsetup.addStringField('Description');
		tsetup.addStringField('Family');
		tsetup.addIntegerField('HideInStore');
		//tsetup.addStringField('LargeImage');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addStringField('ProgramName');
		tsetup.addStringField('ShortDescription');
		tsetup.addStringField('Sku');
		tsetup.addIntegerField('Status');
		tsetup.addIntegerField('Taxable');
	},
	CreditCard : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('BillAddress1');
		tsetup.addStringField('BillAddress2');
		tsetup.addStringField('BillCity');
		tsetup.addStringField('BillCountry');
		tsetup.addStringField('BillName');
		tsetup.addStringField('BillState');
		tsetup.addStringField('BillZip');
		tsetup.addStringField('CardType');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('Email');
		tsetup.addStringField('ExpirationMonth');
		tsetup.addStringField('ExpirationYear');
		tsetup.addStringField('FirstName');
		tsetup.addStringField('Last4');
		tsetup.addStringField('LastName');
		tsetup.addStringField('MaestroIssueNumber');
		tsetup.addIntegerField('NameOnCard');
		tsetup.addStringField('PhoneNumber');
		tsetup.addStringField('ShipAddress1');
		tsetup.addStringField('ShipAddress2');
		tsetup.addStringField('ShipCity');
		tsetup.addStringField('ShipCompanyName');
		tsetup.addStringField('ShipCountry');
		tsetup.addStringField('ShipFirstName');
		tsetup.addStringField('ShipLastName');
		tsetup.addStringField('ShipMiddleName');
		tsetup.addStringField('ShipName');
		tsetup.addStringField('ShipPhoneNumber');
		tsetup.addStringField('ShipState');
		tsetup.addStringField('ShipZip');
		tsetup.addStringField('StartDateMonth');
		tsetup.addStringField('StartDateYear');
		tsetup.addIntegerField('Status');
	},
	DataFormField : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('DataType');
		tsetup.addStringField('DefaultValue');
		tsetup.addStringField('FormId');
		tsetup.addFKField('GroupId', 'DataFormGroup');
		tsetup.addStringField('Label');
		tsetup.addIntegerField('ListRows');
		tsetup.addStringField('Name');
		tsetup.addStringField('Values');
	},
	DataFormGroup : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('Name');
		tsetup.addFKField('TabId', 'DataFormTab');
	},
	DataFormTab : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('FormId');
		tsetup.addStringField('TabName');
	},
	Expense : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('DateIncurred');
		tsetup.addFloatField('ExpenseAmt');
		tsetup.addStringField('ExpenseType');
		tsetup.addIntegerField('TypeId');
	},
	FileBox : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('Extension');
		tsetup.addStringField('FileName');
		tsetup.addStringField('FileSize');
		tsetup.addIntegerField('Public');
	},
	GroupAssign : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('Admin');
		tsetup.addFKField('GroupId', 'UserGroup');
		tsetup.addFKField('UserId', 'User');
	},
	Invoice : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('AffiliateId', 'Affiliate');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addIntegerField('CreditStatus');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('Description');
		tsetup.addFloatField('InvoiceTotal');
		tsetup.addStringField('InvoiceType');
		tsetup.addFKField('JobId', 'Job');
		tsetup.addStringField('LeadAffiliateId');
		tsetup.addIntegerField('PayPlanStatus');
		tsetup.addIntegerField('PayStatus');
		tsetup.addStringField('ProductSold');
		tsetup.addStringField('PromoCode');
		tsetup.addIntegerField('RefundStatus');
		tsetup.addIntegerField('Synced');
		tsetup.addFloatField('TotalDue');
		tsetup.addFloatField('TotalPaid');
	},
	InvoiceItem : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('CommissionStatus');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('Description');
		tsetup.addFloatField('Discount');
		tsetup.addFloatField('InvoiceAmt');
		tsetup.addFKField('InvoiceId', 'Invoice');
		tsetup.addFKField('OrderItemId', 'OrderItem');
	},
	InvoicePayment : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('Amt');
		tsetup.addFKField('InvoiceId', 'Invoice');
		tsetup.addStringField('PayDate');
		tsetup.addIntegerField('PayStatus');
		tsetup.addFKField('PaymentId', 'Payment');
		tsetup.addIntegerField('SkipCommission');
	},
	Job : tsetup => 
	{
		tsetup.frontEndName('Order');
		tsetup.addPKField('Id');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('DueDate');
		tsetup.addStringField('JobNotes');
		tsetup.addFKField('JobRecurringId', 'RecurringOrder');
		tsetup.addStringField('JobStatus');
		tsetup.addStringField('JobTitle');
		tsetup.addIntegerField('OrderStatus');
		tsetup.addIntegerField('OrderType');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addStringField('ShipCity');
		tsetup.addStringField('ShipCompany');
		tsetup.addStringField('ShipCountry');
		tsetup.addStringField('ShipFirstName');
		tsetup.addStringField('ShipLastName');
		tsetup.addStringField('ShipMiddleName');
		tsetup.addStringField('ShipPhone');
		tsetup.addStringField('ShipState');
		tsetup.addStringField('ShipStreet1');
		tsetup.addStringField('ShipStreet2');
		tsetup.addStringField('ShipZip');
		tsetup.addStringField('StartDate');
		tsetup.dataFormId(-9);
	},
	JobRecurringInstance : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('AutoCharge');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('Description');
		tsetup.addStringField('EndDate');
		tsetup.addFKField('InvoiceItemId', 'InvoiceItem');
		tsetup.addFKField('RecurringId', 'RecurringOrder');
		tsetup.addStringField('StartDate');
		tsetup.addIntegerField('Status');
	},
	Lead : tsetup => 
	{
		tsetup.frontEndName('Opportunity');
		tsetup.addPKField('Id');
		tsetup.addFKField('AffiliateId', 'Affiliate');
		tsetup.addFKField('ContactID', 'Contact');
		tsetup.addFKField('CreatedBy', 'User');
		tsetup.addDateField('DateCreated');
		tsetup.addDateField('DateInStage');
		tsetup.addDateField('EstimatedCloseDate');
		tsetup.addBooleanField('IncludeInForecast');
		tsetup.addDateField('LastUpdated');
		tsetup.addFKField('LastUpdatedBy', 'User');
		tsetup.addStringField('Leadsource');
		tsetup.addFloatField('MonthlyRevenue');
		tsetup.addStringField('NextActionDate');
		tsetup.addStringField('NextActionNotes');
		tsetup.addStringField('Objection');
		tsetup.addStringField('OpportunityNotes');
		tsetup.addStringField('OpportunityTitle');
		tsetup.addFloatField('OrderRevenue');
		tsetup.addFloatField('ProjectedRevenueHigh');
		tsetup.addFloatField('ProjectedRevenueLow');
		tsetup.addFKField('StageID', 'Stage');
		tsetup.addFKField('StatusID', 'Status');
		tsetup.addFKField('UserID', 'User');
		tsetup.dataFormId(-4);
		tsetup.alias('opportunity', 'o');

	},
	LeadSource : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('CostPerLead');
		tsetup.addStringField('Description');
		tsetup.addStringField('EndDate');
		tsetup.addFKField('LeadSourceCategoryId', 'LeadSourceCategory');
		tsetup.addStringField('Medium');
		tsetup.addStringField('Message');
		tsetup.addStringField('Name');
		tsetup.addStringField('StartDate');
		tsetup.addStringField('Status');
		tsetup.addStringField('Vendor');
	},
	LeadSourceCategory : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('Description');
		tsetup.addIntegerField('Name');
	},
	LeadSourceExpense : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('Amount');
		tsetup.addStringField('DateIncurred');
		tsetup.addFKField('LeadSourceId', 'LeadSource');
		tsetup.addFKField('LeadSourceRecurringExpenseId', 'LeadSourceRecurringExpense');
		tsetup.addStringField('Notes');
		tsetup.addStringField('Title');
	},
	LeadSourceRecurringExpense : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('Amount');
		tsetup.addStringField('EndDate');
		tsetup.addFKField('LeadSourceId', 'LeadSource');
		tsetup.addStringField('NextExpenseDate');
		tsetup.addStringField('Notes');
		tsetup.addStringField('StartDate');
		tsetup.addStringField('Title');
	},
	MtgLead : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('ActualCloseDate');
		tsetup.addStringField('ApplicationDate');
		tsetup.addStringField('CreditReportDate');
		tsetup.addStringField('DateAppraisalDone');
		tsetup.addStringField('DateAppraisalOrdered');
		tsetup.addStringField('DateAppraisalReceived');
		tsetup.addStringField('DateRateLockExpires');
		tsetup.addStringField('DateRateLocked');
		tsetup.addStringField('DateTitleOrdered');
		tsetup.addStringField('DateTitleReceived');
		tsetup.addStringField('FundingDate');
	},
	OrderItem : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('CPU');
		tsetup.addStringField('ItemDescription');
		tsetup.addStringField('ItemName');
		tsetup.addIntegerField('ItemType');
		tsetup.addStringField('Notes');
		tsetup.addFKField('OrderId', 'Job');
		tsetup.addFloatField('PPU');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addIntegerField('Qty');
		tsetup.addFKField('SubscriptionPlanId', 'SubscriptionPlan');
	},
	Payment : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('ChargeId', 'CCharge');
		tsetup.addIntegerField('Commission');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addFKField('InvoiceId', 'Invoice');
		tsetup.addFloatField('PayAmt');
		tsetup.addStringField('PayDate');
		tsetup.addStringField('PayNote');
		tsetup.addStringField('PayType');
		tsetup.addStringField('RefundId');
		tsetup.addIntegerField('Synced');
		tsetup.addFKField('UserId', 'User');
	},
	PayPlan : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('AmtDue');
		tsetup.addStringField('DateDue');
		tsetup.addFloatField('FirstPayAmt');
		tsetup.addStringField('InitDate');
		tsetup.addFKField('InvoiceId', 'Invoice');
		tsetup.addStringField('StartDate');
		tsetup.addIntegerField('Type');
	},
	PayPlanItem : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('AmtDue');
		tsetup.addFloatField('AmtPaid');
		tsetup.addStringField('DateDue');
		tsetup.addFKField('PayPlanId', 'PayPlan');
		tsetup.addIntegerField('Status');
	},
	Product : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('BottomHTML');
		tsetup.addIntegerField('CityTaxable');
		tsetup.addIntegerField('CountryTaxable');
		tsetup.addStringField('Description');
		tsetup.addIntegerField('HideInStore');
		tsetup.addIntegerField('InventoryLimit');
		tsetup.addStringField('InventoryNotifiee');
		tsetup.addIntegerField('IsPackage');
		//tsetup.addStringField('LargeImage');
		tsetup.addIntegerField('NeedsDigitalDelivery');
		tsetup.addStringField('ProductName');
		tsetup.addFloatField('ProductPrice');
		tsetup.addIntegerField('Shippable');
		tsetup.addStringField('ShippingTime');
		tsetup.addStringField('ShortDescription');
		tsetup.addStringField('Sku');
		tsetup.addIntegerField('StateTaxable');
		tsetup.addIntegerField('Status');
		tsetup.addIntegerField('Taxable');
		tsetup.addStringField('TopHTML');
		tsetup.addFloatField('Weight');
	},
	ProductCategory : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('CategoryDisplayName');
		//tsetup.addStringField('CategoryImage');
		tsetup.addIntegerField('CategoryOrder');
		tsetup.addFKField('ParentId', 'ProductCategory');
	},
	ProductCategoryAssign : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('ProductCategoryId', 'ProductCategory');
		tsetup.addFKField('ProductId', 'Product');
	},
	ProductInterest : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFloatField('DiscountPercent');
		tsetup.addStringField('ObjType');
		tsetup.addStringField('ObjectId');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addStringField('ProductType');
		tsetup.addIntegerField('Qty');
	},
	ProductInterestBundle : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('BundleName');
		tsetup.addStringField('Description');
	},
	ProductOption : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('AllowSpaces');
		tsetup.addStringField('CanContain');
		tsetup.addIntegerField('CanEndWith');
		tsetup.addStringField('CanStartWith');
		tsetup.addIntegerField('IsRequired');
		tsetup.addStringField('Label');
		tsetup.addIntegerField('MaxChars');
		tsetup.addIntegerField('MinChars');
		tsetup.addIntegerField('Name');
		tsetup.addStringField('OptionType');
		tsetup.addIntegerField('Order');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addIntegerField('TextMessage');
	},
	ProductOptValue : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('IsDefault');
		tsetup.addStringField('Label');
		tsetup.addIntegerField('Name');
		tsetup.addIntegerField('OptionIndex');
		tsetup.addFloatField('PriceAdjustment');
		tsetup.addFKField('ProductOptionId', 'ProductOption');
		tsetup.addStringField('Sku');
	},
	RecurringOrder : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('AffiliateId', 'Affiliate');
		tsetup.addIntegerField('AutoCharge');
		tsetup.addFloatField('BillingAmt');
		tsetup.addStringField('BillingCycle');
		tsetup.addStringField('CC1');
		tsetup.addStringField('CC2');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('EndDate');
		tsetup.addIntegerField('Frequency');
		tsetup.addStringField('LastBillDate');
		tsetup.addStringField('LeadAffiliateId');
		tsetup.addIntegerField('MaxRetry');
		tsetup.addStringField('MerchantAccountId');
		tsetup.addStringField('NextBillDate');
		tsetup.addIntegerField('NumDaysBetweenRetry');
		tsetup.addFKField('OriginatingOrderId', 'Job');
		tsetup.addStringField('PaidThruDate');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addFKField('ProgramId', 'CProgram');
		tsetup.addStringField('PromoCode');
		tsetup.addIntegerField('Qty');
		tsetup.addStringField('ReasonStopped');
		tsetup.addIntegerField('ShippingOptionId');
		tsetup.addStringField('StartDate');
		tsetup.addIntegerField('Status');
		tsetup.addFKField('SubscriptionPlanId', 'SubscriptionPlan');
	},
	Referral : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addFKField('AffiliateId', 'Affiliate');
		tsetup.addFKField('ContactId', 'Contact');
		tsetup.addStringField('DateExpires');
		tsetup.addStringField('DateSet');
		tsetup.addStringField('IPAddress');
		tsetup.addStringField('Info');
		tsetup.addStringField('Source');
		tsetup.addIntegerField('Type');
		tsetup.dataFormId(-3);
	},
	SavedFilter : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('FilterName');
		tsetup.addStringField('ReportStoredName');
		//WHY WHY WHY WHY WHY IS THIS A STRINGA?SDFASDFASDF?ASDF ASDLFKIA S:DFLAK SD:FLA KSDF:L AKSD:FL AKSDFL :KSD
		//And why does it INCLUDE A COMMA IN RESULTS FROM XMLRPC?ASD!  What are you thinkingk?
		tsetup.addStringField('UserId');
	},
	Stage : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('StageName');
		tsetup.addIntegerField('StageOrder');
		tsetup.addIntegerField('TargetNumDays');
	},
	StageMove : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('CreatedBy');
		tsetup.addStringField('DateCreated');
		tsetup.addStringField('MoveDate');
		tsetup.addStringField('MoveFromStage');
		tsetup.addStringField('MoveToStage');
		tsetup.addFKField('OpportunityId', 'Lead');
		tsetup.addStringField('PrevStageMoveDate');
		tsetup.addFKField('UserId', 'User');
	},
	Status : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('StatusName');
		tsetup.addIntegerField('StatusOrder');
		tsetup.addIntegerField('TargetNumDays');
	},
	SubscriptionPlan : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addBooleanField('Active');
		tsetup.addStringField('Cycle');
		tsetup.addIntegerField('Frequency');
		tsetup.addFloatField('PlanPrice');
		tsetup.addFloatField('PreAuthorizeAmount');
		tsetup.addFKField('ProductId', 'Product');
		tsetup.addBooleanField('Prorate');
	},
	Template : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('Categories');
		tsetup.addStringField('PieceTitle');
		tsetup.addStringField('PieceType');
	},
	TicketStage : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('StageName');
	},
	TicketType : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('CategoryId');
		tsetup.addStringField('Label');
	},
	User : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addStringField('City');
		tsetup.addStringField('Email');
		tsetup.addStringField('EmailAddress2');
		tsetup.addStringField('EmailAddress3');
		tsetup.addStringField('FirstName');
		tsetup.addStringField('HTMLSignature');
		tsetup.addStringField('LastName');
		tsetup.addStringField('MiddleName');
		tsetup.addStringField('Nickname');
		tsetup.addBooleanField('Partner');
		tsetup.addStringField('Phone1');
		tsetup.addStringField('Phone1Ext');
		tsetup.addStringField('Phone1Type');
		tsetup.addStringField('Phone2');
		tsetup.addStringField('Phone2Ext');
		tsetup.addStringField('Phone2Type');
		tsetup.addStringField('PostalCode');
		tsetup.addStringField('Signature');
		tsetup.addStringField('SpouseName');
		tsetup.addStringField('State');
		tsetup.addStringField('StreetAddress1');
		tsetup.addStringField('StreetAddress2');
		tsetup.addStringField('Suffix');
		tsetup.addStringField('Title');
		tsetup.addStringField('ZipFour1');
		tsetup.alias('u');

	},
	UserGroup : tsetup => 
	{
		tsetup.addPKField('Id');
		tsetup.addIntegerField('Name');
		tsetup.addFKField('OwnerId', 'User');
	},
};
