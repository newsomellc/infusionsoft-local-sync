/**
 * Just sets up infusionsoft to use the passport auth system.  I'm sure if you 
 * wanted to you could roll your own.
 *
 * This function is curried twice to inject dependencies.
 * First pass gives it the inf core object.
 * Second pass gets a reference to expressjs.
 * 
 * It then instantiates and returns an app that can be used directly in express
 * (and to register webhooks).
 */
'use strict';
const debug  = require('debug')('infusionsoft-local-sync:webhooks');

module.exports = (inf, config) =>
{
	let self = {};

	let Express;
	try
	{
		Express = require('express');
	}
	catch (e)
	{
		if (e.code === 'MODULE_NOT_FOUND')
			throw Error('Error: Express.js not installed (or not accessible) in current project.  You can install it with `npm i express`.');
		else
			throw e;
	}
	let app = Express();

	app.post(new RegExp('^/([a-z]*)$'), (req, res, next) =>
	{
		
	});

	return self;
};
