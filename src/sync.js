/**
 * The actual sync job that can be run.
 */
'use strict';
const Debug  = require('debug');
const debug  = Debug('infusionsoft-local-sync:sync');
const Moment = require('moment');

module.exports = (ils, config) => 
{
	let self = Object.create(null);

	let retry_count   = config.sync.retry_count || 5;

	if (!ils._db)
		throw Error('No local database has been set up.');
	let db = ils._db;

	/**
	 * Starts a manual sync operation.
	 */ 
	self.sync = async (table, truncate, force=false) =>
	{
		ils.trigger('log-begin', `Starting ${table} sync operation`);

		let syncmeta = await self.getMeta(table);
		syncmeta.sync_interval = syncmeta.sync_interval || config.sync.interval || 5;
		let page_size = syncmeta.sync_page_size;
		
		if (syncmeta.syncing && !force)
			throw Error(table + ' operation of type ' + syncmeta.extra.operation + ' appears to already be running. Use --force to override this check.');

		if (truncate)
		{
			ils.trigger('log-notice', 'Truncate flag set-- will clear database and reset fail counter and resumption meta');
			syncmeta.extra.failures         = 0;
			syncmeta.extra.resume_page      = 0;
			syncmeta.extra.resume_page_size = page_size;
			syncmeta.last_updated           = null;
			syncmeta.highest_id             = null;

		}

		if ( table === 'ContactGroupAssign' && ils._config.sync.kludge_contactgroupassign )
		{
			ils.trigger('log-fatal', 'Cannot sync ContactGroupAssign while config option `sync.kludge_contactgroupassign` is set.');
			throw Error('not syncing-- direct ContactGroupAssign queries disabled.');

		}

		if (syncmeta.extra && (syncmeta.extra.resume_page && syncmeta.extra.resume_page_size !== page_size))
		{
			ils.trigger('log-fatal', 'page size variable has changed since last unfinished sync-- cannot resume. Run', require('chalk').yellow(`ils sync ${table} --truncate`), 'to restart sync operation.');
			throw Error('cannot resume sync-- sync page size has changed.');
		}

		if (force)
			ils.trigger('log-notice', 'Force flag set-- will not abort due to previous failures');

		if (syncmeta.extra.failures >= retry_count && !force)
		{
			ils.trigger('log-fatal', 'Previous',
				(syncmeta.extra.failures > 1 ? (syncmeta.extra.failures + ' sync operations') : 'sync operation'),
				'failed. Please run', 
				require('chalk').bold.yellow(`ils sync ${table} --truncate --force`),
				'to resolve this.');
			throw Error('not syncing-- too many failures.');
		};

		if (syncmeta.extra.failures > 0)
		{
			ils.trigger('log-warn', 
				'previous '
				+ (syncmeta.extra.failures > 1 ? (syncmeta.extra.failures + ' sync operations') : 'sync operation')
				+ ' failed. If issues persist try running '
				+ require('chalk').yellow(`ils sync ${table} --truncate`));
		}


		let src = new InfusionSourceStream(ils, syncmeta, truncate);
		let dst = new PostgresDestStream(ils._db, ils, syncmeta, truncate);

		let ps = [];

		ps.push(new Promise((res, rej) => src.on('ready', res)));
		ps.push(new Promise((res, rej) => dst.on('ready', res)));

		debug('source and dest streams both report ready, proceeding.');

		await Promise.all(ps);
		
		if (syncmeta.needsSyncFromBeginning() && !truncate)
		{
			ils.trigger('log-error', 'Complete resync is necessary, enabling.');
			truncate = true;
		}
		if (syncmeta.last_updated)
			ils.trigger('log-notice', 'Last updated:', syncmeta.last_updated.toISOString());

		if (syncmeta.extra.resume_page)
			ils.trigger('log-notice', 'Resuming incomplete sync from page ', syncmeta.extra.resume_page + 1, 'for table', table);
		else if (truncate)
			ils.trigger('log-notice', 'Starting complete sync', 'for table', table);
		else
			ils.trigger('log-notice', 'Starting incremental sync', 'for table', table);

		src.pipe(dst);

		return new Promise((res, rej) =>
		{
			let errcb = (err) =>
			{
				if (!syncmeta.extra.failure_history)
					syncmeta.extra.failure_history = [];

				syncmeta.extra.failure_history.push({message: err.toString(), stack : err.stack, operation:syncmeta.extra.operation});
				syncmeta.extra.failures        = syncmeta.extra.failures + 1 || 1;
				syncmeta.syncing               = false;

				dst.removeListener('finish', res);

				syncmeta.writeToDb().then(() =>	rej(err));
			};
			src.on('error', errcb);
			dst.on('error', errcb);
			dst.on('finish', res);
		});
	};

	self.isUpToDate = table => self.getMeta(table).then(async syncmeta =>
	{
		return Moment(syncmeta.last_sync).add(syncmeta.sync_interval, 'm') > Moment(new Date()) && !syncmeta.extra.resume_page;
	});

	/**
	 * Gets metadata for the sync table, creates it if it doesn't exist.
	 */
	self.getMeta = async table =>
	{
		let syncmeta = await db('_syncmeta').where('table', '=', table).first();

		if (!syncmeta)
		{
			syncmeta = (await db('_syncmeta').insert(
			{
				table          : table,
				local_count    : 0,
				remote_count   : await ils.query[table].count(),
				sync_page_size : config.sync.page_size || 1000,
			}).returning('*'))[0];
		}
		Object.defineProperty(syncmeta, 'writeToDb', 
		{
			value : function ()
			{
				let p = db('_syncmeta')
					.where('table','=',this.table)
					.update(this);
				debug('saving syncmeta');
				return p;
			},
			writeable: false,
		});
		Object.defineProperty(syncmeta, 'needsSyncFromBeginning', 
		{
			value : function ()
			{
				 //|| this.last_updated is more than one day how do you do that in js?
				return this.sync_field === 'Id' || (!this.last_sync && !this.extra.resume_page);
			},
			writeable : false,
		});
		Object.defineProperty(syncmeta, 'sync_field', 
		{
			get : function () 
			{
				if (ils.schema[syncmeta.table].fields.indexOf('LastUpdated') > -1)
					return 'LastUpdated';
				else if (ils.schema[syncmeta.table].fields.indexOf('DateCreated') > -1)
					return 'DateCreated';
				else if (ils.schema[syncmeta.table].fields.indexOf('Id') > -1)
					return 'Id';
				else
					throw Error(`Table ${this.table} cannot be synced-- it has no usable value for synchronization.`);
			},
			writeable : false,
		});

		let latest_segment_sync_tip;
		Object.defineProperty(syncmeta, 'latest_segment_sync_tip', 
		{
			get : () => latest_segment_sync_tip,
			set : nv => latest_segment_sync_tip = nv,
		});
		Object.defineProperty(syncmeta, 'fields', 
		{
			get : function ()
			{
				return ils.schema[this.table].fields;
			},
			writeable : false,
		});

		return syncmeta;
	};

	/**
	 * Finds and removes entries that no longer exist on the remote db from the local db
	 */
	self.prune = async (table, force) =>
	{
		ils.trigger('log-begin', `Starting ${table} prune operation`);

		let syncmeta = await self.getMeta(table);

		if (syncmeta.syncing && !force)
			throw Error(table + ' operation of type ' + syncmeta.extra.operation + 'appears to already be running. Use --force to override this check.');
		else if (force)
			ils.trigger('log-notice', 'Force flag set-- will not abort due to previous failures');

		syncmeta.remote_count      = await ils.query[table].remote().count();
		syncmeta.local_count       = await ils.query[table].local().count();
		syncmeta.syncing           = true;
		syncmeta.extra.operation   = 'prune';
		syncmeta.extra.resume_page = undefined;
		syncmeta.extra.page_size   = undefined;

		let page_size       = 1000;//syncmeta.sync_page_size;
		let tot_pages       = Math.ceil(syncmeta.local_count/page_size);
		let tot_discrepancy = syncmeta.local_count - syncmeta.remote_count;

		await syncmeta.writeToDb();

		if (tot_discrepancy === 0)
			ils.trigger('log-info', 'No discrepancy in raw count currently.  You must be doing something right. Running prune operation anyway.');
		else
			ils.trigger('log-info', 'Current total discrepancy: ', Math.abs(tot_discrepancy), tot_discrepancy > 0 ? '(local database greater)' : '(remote database greater)')


		if (table === 'ContactGroupAssign')
		{
			//Since ContactGroupAssign is a massive table and it doesn't have a unique single PK field, it makes more sense to prune this way.
			ils.trigger('log-error', 'pruning process for tag assignments (ContactGroupAssign) runs as part of Contact prune/sync operations.  Running now.');
			await self.prune('Contact');
			await self.sync('Contact');
			return;
		}


		if (tot_discrepancy < 0)
			ils.trigger('log-error', 'local database has fewer total entries than local-- recommend running', require('chalk').yellow('ils sync', table), 'to download missing entries.');

		let dead_list = [];

		for (var i = 0; i * page_size < syncmeta.local_count; i++)
		{
			let lids = await ils.query[table]
				.select('Id')
				.orderBy('Id', 'asc')
				.page(i)
				.limit(page_size)
				.local()
				.execute();

			lids = lids.map(c => c.Id);

			let rids = await ils.query[table]
				.select('Id')
				.orderBy('Id', 'asc')
				.whereIn('Id', lids)
				.remote()
				.execute();
		
			rids = rids.map(c => c.Id);

			let page_discrepancy = lids.length - rids.length;

			if (isNaN(page_discrepancy))
				throw Error('Invalid data-- discrepancies is not a number!');

			if (page_discrepancy > 0)
			{
				let found_discrepancy = 0;
				ils.trigger('log-notice', 'Page', i+1, 'of', tot_pages, 'has', page_discrepancy, 'discrepancies.');
				let lcntr = 0;
				let rcntr = 0;
				do
				{
					let rid = rids[rcntr];
					let lid = lids[lcntr];

					if (lid !== rid)
					{
						found_discrepancy++;
						dead_list.push(lid);
						lcntr++;
					}
					else
					{
						rcntr++;
						lcntr++;
					}
				}
				while (lcntr < lids.length);

				if (page_discrepancy !== found_discrepancy)
					ils.trigger('log-error', 'sanity check: did not find expected number of discrepancies for current page, count by aggregation:', page_discrepancy, 'count by iteration:', found_discrepancy);
			}
			else if (page_discrepancy < 0)
			{
				//while having more remote results than local IS indeed possible for the tables as a whole,
				//it is not possible on individual segments unless something utterly inane is happening like
				//more than one entry with the same PK.
				debug('More remote results than local-- impossible error, this should never happen.');
				throw Error('Programming error: more remote results than local.');
			}
			else if (page_discrepancy === 0)
			{
				ils.trigger('log-info', 'Page', i+1, 'of', tot_pages, 'has no discrepancies.');
			}
		}
		if (dead_list.length)
		{
			ils.trigger('log-notice', 'deleting entries from the local database which do not exist remotely');
			let to_delete = await ils._db(table).select('*').whereIn('Id', dead_list);

			ils.trigger('notify-delete', 'pruned entries', to_delete);

			await ils._db(table).delete('*').whereIn('Id', dead_list);
		}
		else
		{
			ils.trigger('log-notice', 'no discrepancies found to delete');
		}

		syncmeta.remote_count          = await ils.query[table].remote().count();
		syncmeta.local_count           = await ils.query[table].local().count();
		syncmeta.syncing               = false;
		syncmeta.extra.operation       = undefined;
		syncmeta.extra.failures        = 0;
		syncmeta.extra.failure_history = undefined;
		await syncmeta.writeToDb();

		let finl_discrepancy = syncmeta.local_count - syncmeta.remote_count;
		if (finl_discrepancy !== 0)
		{
			ils.trigger('log-error',
				'Discrepancy remains: ', Math.abs(finl_discrepancy),
				finl_discrepancy > 0 ? '(local database greater)' : '(remote database greater)',
				'Run', require('chalk').yellow('ils sync', table, '-truncate'),
				'to clear local database and resync if issues persist.');
		}


		ils.trigger('log-success', require('chalk').bold.green('Finished pruning table'), require('chalk').bold.yellow(table));
	};
	return self;
};

class InfusionSourceStream extends require('stream').Readable
{
	constructor (ils, syncmeta, force_truncate)
	{
		super({objectMode : true});
		this.ils               = ils;
		this.syncmeta          = syncmeta;
		this.table             = syncmeta.table;
		this.page              = syncmeta.extra.resume_page || 0;
		this.truncate          = force_truncate || this.syncmeta.needsSyncFromBeginning() || syncmeta.extra.resume_page;
		this.sync_page_size    = syncmeta.sync_page_size;
		this.sync_field        = syncmeta.sync_field;
		this.table_fields      = ils.schema[this.table].fields;
		this.last_sync_tip     = syncmeta.sync_field === 'Id' ? syncmeta.highest_id : syncmeta.last_updated;
		this.entry_counter     = 0;

 		//We use this further down to make sure that fields that inf gives us blank are properly nulled in the database
		this.null_entry        = Object.create(null);
		this.table_fields.forEach(fname => 
		{
			if (fname[0] === '_') //Not necessary for CFs since the JSON gets replaced whole
				return;
			this.null_entry[fname] = null;
		});

		let ps = [];
		ps.push(ils.q[this.table].remote().count().then(async count =>
		{
			this.syncmeta.remote_count = count;
			if (count === 0)
			{
				this.syncmeta.syncing = false;
				await this.syncmeta.writeToDb();
				throw Error('Infusionsoft table has no entries to sync-- nothing to do.');
			}
			return this.syncmeta.writeToDb();
		}).catch(err => this.destroy(err)));

		if (ils.schema[this.table].has_custom_fields)
			ps.push(ils.schema.updateCustomFields());

		this.ils.trigger('log-debug', 'Previous sync tip: '+ this.last_sync_tip);

		Promise.all(ps)
			.then(result => this.emit('ready'))
			.catch(err => this.destroy(err));
	}

	async _read ()
	{
		let page      = this.page++;
		let direction = this.truncate ? 'asc' : 'desc';
		
		if (this.truncate)
		{
			if (page * this.sync_page_size >= this.syncmeta.remote_count)
			{
				this.ils.trigger('log-debug', 'Page beyond remote table length, end of sync.');
				this.ils.trigger('log-notice', 'Downloaded all available entries.');
				this._wrapUp();
				return;
			}
			else
			{
				this.ils.trigger('log-info', 'on page', page+1, 'of', Math.ceil(this.syncmeta.remote_count / this.sync_page_size));
				let tot_pages = Math.ceil(this.syncmeta.remote_count/this.sync_page_size);
				this.ils.trigger('log-info', `Getting segment ${page + 1} of ${tot_pages}...`);
			}
		}
		if (this.thatsAWrap)
			return;
		
		this.ils.trigger('log-trace', 'Got inf read request');
		
		this._punchDown();

		let entries = await this.ils.q[this.table]
			.remote()
			.orderBy(this.sync_field, direction)
			.limit(this.sync_page_size)
			.page(page)
			.execute()
			.catch(err => this.destroy(err));

		this.entry_counter += entries.length;
		this.ils.trigger('log-info', `${this.entry_counter} ${this.syncmeta.table}s downloaded.`);

		if (this.ils.schema[this.table].has_custom_fields)
		{
			this.ils.trigger('log-trace', 'Preprocessing entries for DB.');
			entries = entries.map(entry =>
			{
				let o  = Object.create(null);
				let cf = Object.create(null);
				
				Object.keys(entry).forEach(k =>
				{
					if (k[0] === '_')
						cf[k] = entry[k];
					else
						o[k] = entry[k];
				});

				o.custom_fields = cf;

				return o;
			});
		}

		await this.syncmeta.writeToDb();

		this.ils.trigger('log-trace', `Got page ${page} result`);
		this.push(entries);

		if (this._punchUp())
			return;

		if (this.truncate)
		{
			let prct = Math.round(this.entry_counter / this.syncmeta.remote_count * 10000)/100;
			this.ils.trigger(`Progress: ${prct}%`);
		}

		if (!this.truncate)
		{
			if (this.syncmeta.sync_field === 'Id')
				this.syncmeta.latest_segment_sync_tip = Math.min(entries[entries.length-1].Id, entries[0].Id);
			else
				this.syncmeta.latest_segment_sync_tip = min_date(entries[entries.length-1][this.syncmeta.sync_field], entries[0][this.syncmeta.sync_field]);

			if (this.syncmeta.latest_segment_sync_tip < this.last_sync_tip)
			{
				//TODO: what if we never reach overlap? Rare case but worth guarding against.
				this.ils.trigger('log-notice', 'Reached tip of previous sync: ' + this.last_sync_tip);
				this._wrapUp();
			}
			else if ((page+1) * this.sync_page_size >= this.syncmeta.remote_count)
			{
				//TODO: what if we never reach overlap? Rare case but worth guarding against.
				this.ils.trigger('log-warn', 'Reached end of database before finding tip of previous sync-- all entries imported. Consider running', 
					require('chalk').yellow('ils s', this.syncmeta.table, '-t'),
					'to clean potentially deleted entries.');
				this._wrapUp();
			}
			else
			{
				this.ils.trigger('log-debug', `Got segment ${this.page +1} (incremental)...`);
				this.ils.trigger('log-info',  `Segment sync tip (${this.syncmeta.sync_field}): ${this.syncmeta.latest_segment_sync_tip}`);
			}
		}
	}

	_punchDown ()
	{
		if (this.thatsAWrap)
			throw Error('Segment requested after wrap-up.');
		this.counter = this.counter+1 || 1;
	}

	_punchUp ()
	{
		this.counter--;
		if (this.thatsAWrap && this.counter===0)
		{
			this.push(null);
			this.emit('end');
			return true;
		}
		return false;
	}
	_wrapUp () 
	{
		this.thatsAWrap = true;
		if (this.counter === 0)
		{
			this.push(null);
			this.emit('end');
		}
	}
};

class PostgresDestStream extends require('stream').Writable 
{
	constructor (db, ils, syncmeta, truncate)
	{
		super({objectMode : true});
		this.db            = db;
		this.ils           = ils;
		this.syncmeta      = syncmeta;
		this.table         = syncmeta.table;
		this.page_counter  = 0;
		this.entry_counter = 0;
		this.truncate      = truncate;

		let ps = [];

		this.syncmeta.syncing = true;
		this.syncmeta.extra.operation = truncate ? 'sync-complete' : 'sync-incremental';
		ps.push(this.syncmeta.writeToDb());

		if (truncate)
		{
			this.ils.trigger('log-debug', 'truncating table ' + this.table);
			this.syncmeta.local_count = 0;

			ps.push(db(this.table).truncate());


			if ( this.table === 'Contact' && ils._config.sync.kludge_contactgroupassign )
			{
				this.ils.trigger('log-debug', 'truncating related table ContactGroupAssign');
				ps.push(db('ContactGroupAssign').truncate());
			}	
		}

		Promise.all(ps).then(() => this.emit('ready')).catch(err => this.destroy(err));
	}

	_write(entries, encoding, callback)
	{
		this.ils.trigger('log-trace', 'Got data');
		this.page_counter++;
	
		this.ils.trigger('log-debug', 'Highest ID:', this.syncmeta.highest_id || 'n/a');
		this.ils.trigger('log-debug', 'Last Updated:', this.syncmeta.last_updated);

		this.ils.trigger('log-trace', 'got chunk, length:', entries.length);

		let upsert_here = upsert(this.db);
		let upserts = entries.map(entry => 
		{
			entry = Object.assign(Object.create(null), this.null_entry, entry);

			if (entry.custom_fields)
				entry.custom_fields = JSON.stringify(entry.custom_fields);

			let constraint = this.table === 'ContactGroupAssign' ? ['ContactGroup', 'ContactId'] : ['Id'];

			let main_query = upsert_here(this.table, entry, constraint);


			if ( this.table === "Contact" && this.ils._config.sync.kludge_contactgroupassign )
			{
				let tag_ids = entry.Groups && entry.Groups.split(',');
				
				if (tag_ids)
				{
					let tag_upserts = tag_ids.map( GroupId =>
					{
						let tag_entry = 
						{
							ContactId    : entry.Id,
							GroupId      : GroupId
						};
						return upsert_here('ContactGroupAssign', tag_entry, ['ContactGroup', 'ContactId']);
					});
					return Promise.all([main_query, ...tag_upserts]);
				}
			}
			return main_query;

		});

		Promise.all(upserts).then(async result => 
		{
			this.entry_counter += entries.length;
			this.ils.trigger('log-info', `${this.entry_counter} ${this.syncmeta.table}s written to DB.`);
			this.syncmeta.local_count = (await this.db(this.table).count().first()).count;
			if (this.truncate)
				this.syncmeta.extra.resume_page = this.page_counter;
			await this.syncmeta.writeToDb();

			this.ils.trigger('log-trace', `wrote chunk #${this.page_counter} to db...`);

			callback();
		
		}).catch(callback);
	}

	async _final (callback)
	{
		let orig_id = this.syncmeta.highest_id;
		let orig_lu = this.syncmeta.last_updated;

		let [lcnt, rcnt] = await Promise.all([this.ils.query[this.syncmeta.table].local().count(), this.ils.query[this.syncmeta.table].remote().count()]);
	
		this.syncmeta.local_count  = lcnt;
		this.syncmeta.remote_count = rcnt;

		if (this.syncmeta.remote_count !== this.syncmeta.local_count)
			this.ils.trigger('log-error', 'Discrepancy between local and remote counts-- recommend running', require('chalk').yellow('ils sync', this.syncmeta.table, '--prune'), 'to remove entries deleted on the remote database.');

		if (this.syncmeta.fields.indexOf('Id') > -1)
			this.syncmeta.highest_id = (await this.db(this.syncmeta.table).max('Id').first()).max;
		else
			this.syncmeta.highest_id = null;

		if (this.syncmeta.fields.indexOf('LastUpdated') > -1)
			this.syncmeta.last_updated = (await this.db(this.syncmeta.table).max('LastUpdated').first()).max;
		else if (this.syncmeta.fields.indexOf('DateCreated') > -1)
			this.syncmeta.last_updated = (await this.db(this.syncmeta.table).max('DateCreated').first()).max;
		else
			this.syncmeta.last_updated = null;

		if 
		(
			(orig_id && this.syncmeta.sync_field === 'Id' && this.syncmeta.highest_id == orig_id)
			||
			(orig_lu && this.syncmeta.sync_field !== 'Id' && this.syncmeta.last_updated.toISOString() == orig_lu.toISOString())
		)
		{
			this.ils.trigger('log-notice', 'Last sync tip matches exactly-- you were probably already up-to-date');
		}
		if (this.syncmeta.table === 'Contact')
		{
			this.ils.trigger('log-error', 'Pruning Contact tags (ContactGroupAssign).  This may take several seconds to a few minutes.');
			await this.db.raw(
				`
				DELETE
				FROM "ContactGroupAssign"
					USING "Contact"
				WHERE
					"ContactId" = "Id"
					AND
					(
						NOT "Groups" ~ ('(^|,)' || ("GroupId" :: TEXT) || '(,|$)')
						OR
						"Groups" IS NULL
					);`);
			await this.db.raw(
			`
				DELETE 
				FROM "ContactGroupAssign"
				WHERE "ContactId" IN 
				(
					SELECT "ContactId"
					FROM "ContactGroupAssign" 
						LEFT JOIN "Contact" ON "ContactId" = "Id"
					WHERE "Id" IS NULL
					GROUP BY "ContactId"
				);
			`);
		}
		this.ils.trigger('log-debug', 'final syncmeta update');
		this.syncmeta.last_sync             = new Date();
		this.syncmeta.syncing               = false;
		this.syncmeta.extra.operation       = undefined;
		this.syncmeta.extra.resume_page     = undefined;
		this.syncmeta.extra.page_size       = undefined;
		this.syncmeta.extra.failures        = 0;
		this.syncmeta.extra.failure_history = undefined;

		this.syncmeta.writeToDb().then(() =>
		{
			this.ils.trigger('log-success', require('chalk').bold.green('Finished syncing table'), require('chalk').bold.yellow(this.table));
			callback();
		});
	}
}

const upsert = db => (table, object, constraint) => 
{
	const insert = db(table).insert(object);
	const update = db.queryBuilder().update(object);
	return db.raw(`? ON CONFLICT ("${constraint.join('","')}") DO ? returning *`, [insert, update]).get('rows').get(0);
}


const max_date = (...dates) =>
{
	dates = dates.map(d => d instanceof Date ? d : new Date(d));
	return new Date(Math.max(...dates));
};

const min_date = (...dates) =>
{
	dates = dates.map(d => d instanceof Date ? d : new Date(d));
	return new Date(Math.min(...dates));
};
