/** 
 * Various helper methods to deal with OAuth (to replace passport-infusionsoft
 * plugin as well as allow using OAuth via CLI.)
 */
const REMOTE_HOST = 'accounts.infusionsoft.com';
exports = (ils) =>
{
	let self = Object.create(null);

	self.getGrantUrl = (client_id, redirect_uri) =>
		`https://${REMOTE_HOST}/app/oauth/authorize?redirect_uri=${redirect_uri}&client_id=${client_id}&response_type=code`;

	self.getTokenFromAuthCode = payload => 
		
};