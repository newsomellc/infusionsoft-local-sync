/**
 * A Knex DB migration.
 * Decided against including the raw migrations in a user's project for two reasons-- One because it's not up to the user how the ILS database is constructed, and two because we use a DRY (src/defs.js) approach to setting up everything to do with the table structure etc... 
 * Run migrations for your local database using `ils db migrate`.  
 */
'use strict';
const debug  = require('debug')('infusionsoft-local-sync:db:migrations');

const defs = require('../defs');
const conf = require('../get-ils-config')();

exports.up = (knex, Promise) =>
{
	let promises = [];

	promises.push(knex.schema
		.raw(`CREATE SCHEMA IF NOT EXISTS ${conf.get('sync:schema')};`)
		.raw(`SET search_path=${conf.get('sync:schema')};`));

	Object.keys(defs).forEach(tablename =>
	{
		let p = knex.schema.withSchema(conf.get('sync:schema')).createTable(tablename, table =>
		{
			let builder = Object.create(null);
			builder.addPKField = name =>
			{
				//We don't actually add a constraint here, since infusionsoft can't be relied upon. So we just make it an integer.
				table.integer(name).unsigned().primary();
				//making it unsigned might be a mistake, because in infusionsoft's own database, these keys aren't.  I have no idea why.
			};
			builder.addStringField = name =>
			{
				table.text(name);
			};
			builder.addFKField = (name, ref_table) =>
			{
				//Ditto for FK.  We can at least comment what table it refers to though.
				table.integer(name).unsigned().comment(`references Id on ${ref_table}`);

			};
			builder.addDateField = name =>
			{
				table.dateTime(name);
			};
			builder.addTags = () =>
			{
				table.jsonb('tags');
			};

			builder.frontEndName = name =>
			{
				table.comment(`Front end name: ${name}`);
			};

			builder.addFloatField = name =>
			{
				table.float(name);
			};

			builder.addEnumField = (name, values) =>
			{
				table.enu(name, values);
			};

			builder.addBooleanField = name =>
			{
				table.boolean(name).defaultTo(false);
			};

			builder.addIntegerField = (name) =>
			{
				table.integer(name);
			};

			//Just a NOP for now.
			builder.alias = (...aliases) => {};

			builder.dataFormId = (df_id) => 
			{
				table.jsonb('custom_fields').default('[]');
			};

			//Only needed for tag assignemnts grr
			builder.unique = (cols) =>
			{
				table.unique(cols);
			};

			defs[tablename](builder);
			return table;
		});
		promises.push(p);
	});

	promises.push(knex.schema.withSchema(conf.get('sync:schema')).createTable('_syncmeta', table => 
	{
		table.text('table').primary();
		table.integer('remote_count').unsigned();
		table.integer('local_count').unsigned();
		table.dateTime('last_updated');
		table.integer('highest_id');
		table.integer('sync_interval');
		table.integer('sync_page_size');
		table.boolean('syncing').notNull().defaultsTo(false);
		table.jsonb('extra').default('{}');
		table.dateTime('last_sync');

		return table;
	}));

	return Promise.all(promises);
};

exports.down = (knex, Promise) =>
{
	let promises = [];
	promises.push(knex.schema.raw(`SET search_path=${conf.get('sync:schema')};`));
	Object.keys(defs).forEach(tablename => promises.push(knex.schema.withSchema(conf.get('sync:schema')).dropTable(tablename)));
	promises.push(knex.schema.withSchema(conf.get('sync:schema')).dropTable('_syncmeta'));

	return Promise.all(promises);
};
