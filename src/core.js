'use strict';
const EventEmitter = require('events');
const debug        = require('debug')('ils:core');
module.exports = (request, xmlrpc, knex) => (config, is_cli=false) =>
{
	if (!config)
		config = require('./get-ils-config')().get();

	let ils = Object.create(PROTOTYPE);
	ils._config = config;

	//ILS event emitter.  Using non-standard names so nobody mistakes the core 
	//ils object for a real event emitter.
	ils._events = new EventEmitter();
	ils.listen = (name, callback) => ils._events.on(name, callback);
	ils.trigger = (name, ...args) => ils._events.emit(name, ...args);

	//Set up schema first.  It tells API the basic fields we need.
	ils.schema = require('./schema')(ils, config);
	debug('Got schema OK');

	//infusionsoft XMLRPC/REST API here. Stuff like ContactService etc, which they don't offer in just queries.
	ils.api = require('./api')(request, xmlrpc)(ils, config);

	debug('Got api OK');

	//works like a knex object instance
	ils.query = require('./query')(ils, config);
	ils.q = ils.query;
	debug('Got query OK');

	//knex DB client
	if(config.sync)
	{
		ils._db = require('./db')(knex)(ils, config);
		debug('Got db OK');
		//sync manager.  Sync depends on DB, if you don't have it, you don't want it.
		ils.sync = require('./sync')(ils, config, ils._db);
		debug('Got sync OK');
	}

	if (!is_cli)
	{
		if (config.web.webhooks)
		{
			ils.webhooks = require('./webhooks')(ils, config);
			debug('Got webhooks OK');
		}
	}

	ils.close = () =>
	{
		ils.trigger('close');
	};

	let ready = new Promise(res => ils.listen('ready', res));
	
	//basically a NOP, to keep the code style consistent.
	ils.ready = () => ready;

	if (is_cli === 'NO_SYNC')
	{
		ils.trigger('ready');
		return ils;
	}

	let ps =
	[
		ils.schema.updateCustomFields(is_cli),
		ils.schema.updateLeadSources(is_cli),
		ils.schema.updateTags(is_cli),
		ils.schema.updateUsers(is_cli),
		ils.schema.updateStages(is_cli),
	];

	Promise.all(ps).then(res => 
	{
		debug('got result of init fn');
		ils.trigger('ready');
	});

	return ils;
};

let PROTOTYPE = 
{
	_defs : require('./defs'),
};

module.exports.defs = require('./defs');

