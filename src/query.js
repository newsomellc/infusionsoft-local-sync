/**
 * This module provides a bunch of shortcuts, allowing us to do Knex-style
 * queries against infusionsoft.
 */
'use strict';
const defs   = require('./defs');
const debug  = require('debug')('ils:query');

//let seen_update_warning = Object.create(null);

module.exports = ils => 
{
	let self = Object.create(null);
	self.getUserQ = query_for_user(ils)(self);
	return self.getUserQ();
};

const query_for_user = ils => parent => (user_token, refresh_token) =>
{
	let self = Object.create(parent);

	let api = ils.api.getUserAPI(user_token, refresh_token);

	let taglist = Object.create(null);

	if (user_token)
		self.getCurrentUser = get_current_user(ils, api);

	self.tagAdd    = tag_operation(ils, api)('addToGroup');
	self.tagRemove = tag_operation(ils, api)('removeFromGroup');

	return new Proxy(self,
	{
		get : (target, name) =>
		{
			if (target[name])
				return target[name];

			if (!ils.schema[name])
				throw Error('Unknown table: ' + name);

			name = ils.schema[name].canonical_name;

			target[name] = query_builder(ils, api, self)(name);

			return target[name];
		}
	});
};

/** 
 * Get current user information, including stuff they leave out for some reason.
 */ 
const get_current_user = (ils, api) => async () => 
{
	let user = Object.create(null);
	let user_meta = await api.rest('/oauth/connect/userinfo');

	user_meta     = JSON.parse(user_meta);
	user.id       = parseInt(user_meta.sub);
	user.username = user_meta.infusionsoft_id;

	let user_info = await ils.schema.getUser(user_meta.infusionsoft_id);

	if (!user_info)
		throw Error('Sorry, you aren\'t known to our app.');

	user.first_name = user_info.FirstName;
	user.last_name  = user_info.LastName;
	user.email      = user_info.Email;
	user.phone      = user_info.Phone1;

	return user;
}

/**
 * Add or remove tags.
 */
const tag_operation = (ils, api) => operation => async (tag_name, ...args) =>
{
	args = array_flatten(args, true);
	let tag_id = (await ils.schema.getTag(tag_name)).Id;
	return Promise.all(args.map(contact_id => api.xmlrpc('ContactService.' + operation, contact_id, tag_id)));
};

const query_builder = (ils, api, self) => table =>
{
	if (!table)
		throw Error('Need name of table on which to run queries.');

	const db = ils._db;

	if (!db)
	{
		debug('no local DB configured-- only remote queries available.');
		forceRemote = true;
	}

	let q = Object.create(null);
	let n = () => Object.create(q);

	/**
	 * Builds a select type query.  Adds methods to filter the select.
	 */
	q.select = (..._select) =>
	{
		if (_select.length)
		{
			_select = array_flatten(_select, true);

			let check = _select.filter(f => ils.schema[table].fields.indexOf(f) < 0);
			if (check.length)
				throw Error(`Unknown field(s): '${check.join(',')}'`);
		}
		else
		{
			_select = ils.schema[table].fields.map(f=>f);
		}

		let select   = _select
		let where    = [];
		let first    = false;
		let order_by = table === 'ContactGroupAssign' ? 'ContactGroup' : 'Id';
		let asc      = false;
		let limit    = 1000; //TODO I can chain together queries and concat the returned arrays to return more than this. But is it sane?
		let page     = 0;
	
		let forceRemote  = false;
		let forceLocal   = false;
		let preferRemote = false;

		let q = n();

		Object.defineProperty(q, 'remoteForced', {get: () => forceRemote});
		Object.defineProperty(q, 'localForced', {get: () => forceLocal});

		q.first = () =>
		{
			limit = 1;
			first = true;
			return q;
		};

		q.orderBy = (_order_by, _asc) =>
		{
			order_by = _order_by
			
			switch (_asc)
			{
				case 'asc': 
					asc = true;
					break;
				case 'desc':
					asc = false;
					break;
			}
			return q;
		};

		q.orderByRaw = (...args) =>
		{
			forceLocal = true;
			order_by   = false;
			where.push(['orderByRaw', ...args]);
			return q;
		};

		q.limit = _limit =>
		{

			limit = _limit;
			return q;
		};
		/* Implement this.  Knex is our model, not infusionsoft.  
		q.offset = _offset =>
		{
			q.page(Math.floor(limit/_offset));
			return q;
		};*/

		q.page = _page => 
		{
			page = _page
			return q;
		};

		/**
		 * Where methods.
		 */
		q.where = (...args) =>
		{
			if (['=', 'like',].indexOf(args[1]) < 0)
				forceLocal = true;

			where.push(['where', ...args]);
			return q;
		};

		q.andWhere = q.where;

		q.orWhere = (...args) =>
		{
			forceLocal = true;
			where.push(['orWhere', ...args]);
			return q;
		};

		//where null has to be forced local.  I thought this worked, but the API
		//was coercing all falsies (including empty strings) to zero and matching
		//on that.
		//it's not a true where(Not)Null.
		q.whereNull = (...args) =>
		{
			forceLocal = true;
			where.push(['whereNull', ...args]);
			return q;
		}
		
		q.andWhereNull = (...args) =>
		{
			forceLocal = true;
			where.push(['andWhereNull', ...args]);
			return q;
		}

		q.orWhereNull = (...args) =>
		{
			forceLocal = true;
			where.push(['orWhereNull', ...args]);
			return q;
		};

		q.whereNotNull = (...args) =>
		{
			forceLocal = true;
			where.push(['whereNotNull', ...args]);
			return q;
		}
		
		q.andWhereNotNull = (...args) =>
		{
			forceLocal = true;
			where.push(['andWhereNotNull', ...args]);
			return q;
		}

		q.orWhereNotNull = (...args) =>
		{
			forceLocal = true;
			where.push(['orWhereNotNull', ...args]);
			return q;
		};

		q.whereRaw = (...args) =>
		{
			forceLocal = true;
			where.push(['whereRaw', ...args]);
			return q;
		};

		q.whereIn = (...args) => 
		{
			if (ils.schema[table].types[args[0]] !== 'ID' && ils.schema[table].types[args[0]] !== 22)
				forceLocal = true;
			where.push(['whereIn', ...args]);
			return q;
		};

		q.orWhereIn = (...args) => 
		{
			forceLocal = true;
			where.push(['orWhereIn', ...args]);
			return q;
		};

		q.whereNot = (...args) =>
		{
			forceLocal = true;
			where.push(['whereNot', ...args]);
			return q;
		};

		q.whereBetween = (...args) =>
		{
			forceLocal = true;
			where.push(['whereBetween', ...args]);
			return q;
		};

		q.orWhereBetween = (...args) =>
		{
			forceLocal = true;
			where.push(['orWhereBetween', ...args]);
			return q;
		};

		q.whereNotBetween = (...args) =>
		{
			forceLocal = true;
			where.push(['whereNotBetween', ...args]);
			return q;
		};

		q.orWhereNotBetween = (...args) =>
		{
			forceLocal = true;
			where.push(['orWhereNotBetween', ...args]);
			return q;
		};

		q.whereNotIn = (...args) => 
		{
			forceLocal = true;
			where.push(['whereNotIn', ...args]);
			return q;
		};

		q.orWhereNotIn = (...args) => 
		{
			forceLocal = true;
			where.push(['orWhereNotIn', ...args]);
			return q;
		};

		/**
		 * Execute methods. 
		 */
		q.execute = () =>
		{
			if (forceLocal && forceRemote)
				throw Error('Some query options are incompatible with remote queries-- set up database synchronization to use these options.');

			if (preferRemote && !forceLocal || forceRemote)
				return execute_remote();
			else
				return execute_local();
		};
		let execute_local = (get_count) => new Promise(async (res, rej) =>
		{
			/* This seems like a bad idea.
			if (!await ils.sync.isUpToDate(table))
			{
				if (!seen_update_warning[table])
					ils.trigger('log-warn', table, ' table is not up to date. Some entries may be missing from query results.');
				seen_update_warning[table] = true;
			}
			else
				seen_update_warning[table] = false;*/

			if (!get_count)
			{
				debug('local query:', table);
			}
			let query = db(table);

			where.forEach(call =>
			{
				if (call[1][0] === '_')
				{
					call = call.slice();
					//because, because, because
					let field = call[1];
					let ftype = ils.schema[table].types[field];

					//month and day aren't specific enough to compare to other timestamps.
					if (ftype === 14 || ftype === 13 || ftype === 7) 
						call[1] = db.raw(`(custom_fields->>'${field}')::TIMESTAMP AT TIME ZONE 'UTC'`);
					else
						call[1] = db.raw(`custom_fields->>'${field}'`);

					if (call[3] !== undefined)
					{
						if (call[3] === true)
							call[3] = 1;
						if (call[3] === false)
							call[3] = 0;
						call[3] = call[3].toString();
					}
					else if (call[2])
					{
						if (call[2] === true)
							call[2] = 1;
						if (call[2] === false)
							call[2] = 0;
						call[2] = call[2].toString();
					}
				}
				return query[call[0]](...call.slice(1));
			});

			if (table === 'Contact')
			{
				if (must_tags.length)
				{
					let mt = must_tags.map(t => ils.schema.getTag(t));
					mt = await Promise.all(mt);
					mt = mt.map(t=>t.Id);

					query.whereExists(function()
					{
						this.select('*').from('ContactGroupAssign')
							.whereRaw('"Contact"."Id" = "ContactId" AND "GroupId" IN (??)', [mt]);
					});
				}
				if (cant_tags.length)
				{
					let ct = cant_tags.map(t => ils.schema.getTag(t));
					ct = await Promise.all(ct);
					ct = ct.map(t=>t.Id);

					query.whereNotExists(function()
					{
						this.select('*').from('ContactGroupAssign')
							.whereRaw('"Contact"."Id" = "ContactId" AND "GroupId" IN (??)', [ct]);
					});
				}
			}

			let select_main = select.filter(f => f[0] !== '_');
			let select_cf   = select.filter(f => f[0] === '_');

			if (get_count)
			{
				query.count();
			}
			else
			{
				query.limit(limit).offset(page*limit);
				if (order_by)
					query.orderBy(order_by, asc ? 'ASC' : 'DESC');

				if (select_cf.length)
					select_main.push('custom_fields');
				query.select(...select_main);
			}

			if (first)
				query.first();
			
			query.then(result => res(db_to_js(result, select_cf)));
			query.catch(rej);
		});

		let execute_remote = () => new Promise(async (res, rej) =>
		{
			debug('remote query:',table);

			let apiwhere = table === 'ContactGroupAssign' ? {ContactGroup : '%'} : {Id : '%'};

			where.forEach(call =>
			{
				if (['where', 'andWhere', 'whereIn', 'andWhereIn', 'whereNull', 'andWhereNull'].indexOf(call[0]) < 0)
				 throw Error('Invalid query method for remote: ' + call[0]);

				if (['whereNull', 'andWhereNull'].indexOf(call[0]) >= 0)
					apiwhere[call[2]] = '';
				else
					apiwhere[call[1]] = call[3] || call[2];
			});

			api.xmlrpc('DataService.query', table, limit, page, apiwhere, select, order_by, asc)
				.then(entries =>
				{
					if (first)
						return res(inf_to_js(entries[0]));
					else
						res(entries.map(inf_to_js));
				})
				.catch(rej);
		});

		/**
		 * Count methods. 
		 */
		q.count = () =>
		{
			if (forceLocal && forceRemote)
				throw Error('.remote() method called with incompatible query options-- some options are only available on the local database');

			if (preferRemote && !forceLocal || forceRemote)
				return count_remote();
			else
				return count_local();
		};

		let count_local = async () =>
		{
			debug('local count:', table);
			let res = await execute_local(true);
			return parseInt(res[0].count);
		};

		let count_remote = () =>
		{
			debug('remote count:', table);
			let apiwhere = table === 'ContactGroupAssign' ? {ContactGroup : '%'} : {Id : '%'};

			where.forEach(call =>
			{
				if (['where', 'andWhere', 'whereIn', 'andWhereIn', 'whereNull', 'andWhereNull'].indexOf(call[0]) < 0)
					throw Error('Invalid query method for remote: ' + call[0]);

				if (['whereNull', 'andWhereNull'].indexOf(call[0]) >= 0)
					apiwhere[call[1]] = '';
				else
					apiwhere[call[1]] = call[3] || call[2];
			});


			return api.xmlrpc('DataService.count', table, apiwhere);
		};

		if (table === 'Contact')
		{
			var must_tags = [];
			q.tagged = function (..._must_tags)
			{
				//I just can't make tagged work on remote queries right now
				forceLocal = true;
				must_tags = array_flatten([...must_tags, ..._must_tags], true);
				return q;
			};

			var cant_tags = [];
			q.notTagged = (..._cant_tags) =>
			{
				forceLocal = true;
				cant_tags = array_flatten([...cant_tags, ..._cant_tags], true);
				return q;
			};
		}

		/**
		 * Forces queries to pull from infusionsoft api
		 */
		q.remote = () => 
		{
			if (forceLocal)
				throw Error('Some previously selected query options cannot be used with remote queries.');
			preferRemote = true;
			return q;
		};

		/**
		 * Sets queries to pull from local database.  Used to set a built query 
		 * back to local after having set it to remote.
		 */
		q.local = () => 
		{
			if (forceRemote)
				throw Error('Some previously selected options cannot be used with local queries.');
			preferRemote = false;
			return q;
		};
		q.e = q.execute;
		return q;
	};

	/**
	 * Just a simple get/load.  For individual entries only.  Always remote.
	 */
	q.get = (Id, fields) =>
	{
		if (table === 'ContactGroupAssign')
			throw Error('`ContactGroupAssign` cannot be used with get, delete, update, or upsert queries.  Use the ContactService API for this.');
		if (!Id)
			throw Error(`Need Id of ${table} to get.`);

		let q = new Promise((res, rej) =>
		{
			api.xmlrpc('DataService.load', table, Id, fields || ils.schema[table].fields)
				.then(entry => res(inf_to_js(entry)))
				.catch(rej);
		});

		return q;
	}

	/**
	 * Builds a delete query.
	 */
	q.delete = a1 =>
	{
		if (table === 'ContactGroupAssign')
			throw Error('`ContactGroupAssign` cannot be used with delete, update, or upsert queries.  Use the ContactService API for this.');
		if (!a1)
			throw Error(`Need Id of ${table} to delete.`);

		let fn_mkprom = Id => new Promise((res, rej) =>
		{
			api.xmlrpc('DataService.delete', table, Id)
				.then(res)
				.catch(rej);
		});

		if (Array.isArray(a1))
			return Promise.all(a1.map(fn_mkprom));
		else
			return fn_mkprom(a1);
	};

	/**
	 * Builds an update query.  Always remote (obv).  TODO: trigger a deferred sync operation.
	 */
	q.update = (a1, a2) =>
	{
		if (table === 'ContactGroupAssign')
			throw Error('`ContactGroupAssign` cannot be used with delete, update, or upsert queries.  Use the ContactService API for this.');
		
		let q;
		let fn_mkprom = (entry) => new Promise((res, rej) =>
			api.xmlrpc('DataService.update', table, entry.Id, entry).then(res).catch(rej)
		);

		let multiple = Array.isArray(a1);
		if (multiple)
		{
			var entries = a1;
			if (entries.filter(e => !e.Id).length > 0)
				throw Error(`Massive update requires every ${table} have its Id field set!`);
			
			q = Promise.all(entries.map(fn_mkprom));
		}
		else
		{
			if (a2)
			{
				var entry = a2;
				entry.Id = a1;
			}
			else
			{
				if (!a1.Id)
				{
					throw Error('No `Id` field on update object.  Either pass it as the first parameter, or include it in the object.');
				}
				else
				{
					var entry = a1;
				}
			}
			q = fn_mkprom(entry);
		}

		return q;
	};

	/**
	 * Adds items.   Always remote.  TODO: trigger a deferred sync operation.
	 */
	q.insert = (...a1) =>
	{
		a1 = array_flatten(a1);
	
		if (table === 'ContactGroupAssign')
			throw Error('`ContactGroupAssign` cannot be used with delete, update, or upsert queries.  Use the ContactService API for this.');
		if (!a1)
			throw Error(`At least one ${table} needed to run insert query.`);
		
		let q;
		let fn_mkprom = (entry) => new Promise((res, rej) =>
			api.xmlrpc('DataService.add', table, entry).then(res).catch(rej)
		);

		if (Array.isArray(a1))
			q = Promise.all(a1.map(fn_mkprom));
		else
			q = fn_mkprom(a1);

		return q;
	};

	/**
	 * Just make .select the default query action.
	 */
	q.where        = (...args) => q.select().where(...args);
	q.whereNull    = (...args) => q.select().whereNull(...args);
	q.whereNotNull = (...args) => q.select().whereNotNull(...args);
	q.whereIn      = (...args) => q.select().whereIn(...args);
	q.first        = (...args) => q.select().first(...args);
	q.orderBy      = (...args) => q.select().orderBy(...args);
	q.limit        = (...args) => q.select().limit(...args);
	q.page         = (...args) => q.select().page(...args);
	q.execute      = (...args) => q.select().execute(...args);
	q.remote       = (...args) => q.select().remote(...args);
	q.local        = (...args) => q.select().local(...args);
	q.count        = (...args) => q.select().count(...args);

	if (table === 'Contact')
	{
		q.tagged    = (...args) => q.select().tagged(...args);
		q.notTagged = (...args) => q.select().notTagged(...args);
	}

	return q;
};

/**
 * Utility function.  Flattens an array of arrays of arrays (recursive) into just one long array.
 */
let array_flatten = (a, wrap=false) =>
{
	if (Array.isArray(a))
		if (a.length === 1)
			return array_flatten(a[0], wrap);
		else
			return a.reduce((car,cur) =>
			{
				if (Array.isArray(cur))
					return [...car, ...array_flatten(cur)];
				else
					return [...car, cur];
			}, []);
	else
		if (wrap)
			return [a];
		else
			return a;
};

/**
 * Converts an object to what Infusionsoft understands.
 */
const js_to_inf = entries =>
{
	return entries;
};

/** 
 * converts an object as retrived from the database to the JS representation.
 * de-objectifies all the custom json fields etc.
 */
const db_to_js = (entries, selected_cfs) =>
{
	let convert = e =>
	{
		if (!e)
			return e;
		if (e.custom_fields)
		{
			let o = {};

			Object.keys(e).forEach(f =>
			{
				if (f === 'custom_fields')
					return;
				o[f] = e[f];
			});

			if (selected_cfs.length)
				selected_cfs.forEach(f => o[f] = e.custom_fields[f]);
			else
				Object.assign(o, e.custom_fields);

			return o;
		}
		else
			return Object.assign({}, e);
		return o;
	}
	if (Array.isArray(entries))
		return entries.map(convert);
	else
		return convert(entries);
};

/**
 * Converts an object to its javascript representation.
 */
const inf_to_js = entries =>
{
	let convert = e =>
	{
		let o = {};

		Object.keys(e).forEach(k =>
		{
			let val = e[k];

			if (val === undefined)
				return;

			let t = ils.schema[table].types[k];

			if (t)
			{
				if (t === 17) //list box
					val = val.split(',');
				if (t === 22 && ils.User.cached_vals[val]) //user
					val = ils.User.cached_vals[val];
			}
			o[k] = val;
		});
		return e;
	};

	return entries;
};
