/**
 * Manages the schema-- this includes such things as custom fields, tags, etc. 
 * Stuff we need to know BEFORE doing queries, but which must be resolved from 
 * the API.
 */
const defs = require('./defs');
const debug = require('debug');
const chalk = require('chalk');

module.exports = (ils, config) =>
{
	const debug_fn       = require('debug')('ils:api');
	const debug_fn_trace = require('debug')('(trace)ils:api');

	const debug = (...args) =>
	{
		debug_fn(...args);
		ils.trigger('log-debug', ...args);
	};

	const debug_trace = (...args) =>
	{
		debug_fn_trace(...args);
		ils.trigger('log-trace', ...args);
	};

	const self = Object.create(null);

	let tables_with_custom_fields = [];
	let table_dfid_map = Object.create(null);

	const get_table_def = (table, def_fn) =>
	{
		let tdef = Object.create(null);

		tdef.fields            = [];
		tdef.types             = Object.create(null);
		tdef.canonical_name    = table;
		tdef.aliases           = [table + 's', table.toLowerCase(), table.toLowerCase() + 's'];
		tdef.foreign_keys      = Object.create(null);
		tdef.fields_lc         = Object.create(null);
		tdef.label_map         = Object.create(null);
		tdef.labels            = Object.create(null);
		tdef.has_custom_fields = false;

		let proxy = new Proxy(Object.create(null), 
		{
			get : (target, name) =>
			{

				if (name === 'addPKField')
					return field_name =>
					{
						tdef.types[field_name] = TYPES.ID;
						tdef.fields.push(field_name);
					};

				if (name === 'addFKField')
					return (field_name, reference) =>
					{
						tdef.types[field_name] = TYPES.ID;
						tdef.fields.push(field_name);
						tdef.foreign_keys[reference] = field_name;
					};

				if (['addIntegerField', 'addFloatField'].indexOf(name) > -1)
					return field_name =>
					{
						tdef.types[field_name] = TYPES.NUMBER;
						tdef.fields.push(field_name);
					};

				if (name === 'addBooleanField')
					return field_name =>
					{
						tdef.types[field_name] = TYPES.BOOLEAN;
						tdef.fields.push(field_name);
					};

				if (name == 'addStringField')
					return field_name =>
					{
						tdef.types[field_name] = TYPES.STRING;
						tdef.fields.push(field_name);
					};

				if (name === 'addDateField')
					return field_name =>
					{
						tdef.types[field_name] = TYPES.DATE; 
						tdef.fields.push(field_name);
					};
				
				if (name === 'addEnumField')
					return (field_name, values) =>
					{
						tdef.types[field_name] = TYPES.STRING;
						tdef.fields.push(field_name);
					}; 

				if (name === 'dataFormId')
					return dfid =>
					{
						tables_with_custom_fields.push(table);
						table_dfid_map[dfid]   = tdef;
						tdef.data_form_id      = dfid;
						tdef.has_custom_fields = true; //just to make it EXTRA clear.
					};

				if (name === 'frontEndName')
					return (front_end_name) =>
					{
						tdef.front_end_name = front_end_name;
						tdef.aliases.push(front_end_name);
					};

				if (name === 'alias')
					return (...aliases) =>
					{
						tdef.aliases = [...tdef.aliases, ...aliases];
					};

				if (name === 'addTags' || name === 'unique')
					return () => {};

				throw Error('Unknown definition function: ' + name);
			},
		});

		defs[table](proxy);
		//a map table, to make us case-insensitive on the front end.
		tdef.fields.forEach(f => tdef.fields_lc[f.toLowerCase] = f);

		tdef.normalizeFields = TABLE_NORMALIZE_FIELD_LIST;

		return tdef;
	};

	Object.keys(defs).forEach(table => 
	{
		self[table] = get_table_def(table, defs[table]);
		self[table].aliases.forEach(alias => self[alias] = self[table]);
	});

	const updateCustomFields = async (is_cli) =>
	{
		debug('updating custom fields.');
		if (ils.sync && !await ils.sync.isUpToDate('DataFormField'))
		{
			if (!is_cli)
			{
				ils.trigger('log-error', 'Custom Field table (DataFormField) is outdated, syncing now.');
				await ils.sync.sync('DataFormField');
			}
			else
				ils.trigger('log-warn', 'Custom Field table (DataFormField) is outdated, recommend resyncing with', chalk.bold.yellow('ils sync DataFormField'));

		}
		else
			debug('sync disabled, proceeding using remote data.');


		let reslv = tables_with_custom_fields.map(async table =>
		{
			let tdef = self[table];
			let dfid = tdef.data_form_id;
			
			tdef.fields = tdef.fields.filter(f => f[0] !== '_');

			let dff_list = await ils.query.DataFormField
				.where('FormId', '=', dfid)
				.execute();

			dff_list.forEach(dff =>
			{
				debug_trace('got custom field for table', table, 'named', dff.Name, 'labelled', dff.Label, 'ID:', 'dff.Id');
				tdef.fields.push('_' + dff.Name);
				tdef.label_map[dff.Label] = '_' + dff.Name;
				tdef.labels['_' + dff.Name] = dff.Label;
				tdef.fields_lc = '_' + dff.Name.toLowerCase();
			});
		});
		return await Promise.all(reslv);
	};

	let tag_id_map   = Object.create(null);
	let tag_name_map = Object.create(null);
	const updateTags = async (is_cli) =>
	{
		if (ils.sync && !await ils.sync.isUpToDate('ContactGroup')) 
		{
			if (!is_cli)
			{
				ils.trigger('log-error', 'Tags table (CountactGroup) is outdated, syncing now.');
				await ils.sync.sync('ContactGroup');
			}
			else
				ils.trigger('log-warn', 'Tags table (CountactGroup) is outdated, recommend resyncing with', chalk.bold.yellow('ils sync ContactGroup'));
		}
		else
			debug('sync disabled, proceeding using remote data.');

		let tags = await ils.query.ContactGroup.execute();

		tags.forEach(tag =>
		{
			tag_id_map[tag.Id] = tag;
			tag_name_map[tag.GroupName] = tag;
		});
	};

	let lead_source_id_map   = Object.create(null);
	let lead_source_name_map = Object.create(null);
	const updateLeadSources = async (is_cli) =>
	{
		if (ils.sync && !await ils.sync.isUpToDate('LeadSource'))
		{
			if (!is_cli)
			{
				ils.trigger('log-error', 'Lead Source Table is outdated, syncing now.');
				await ils.sync.sync('LeadSource');
			}
			else
				ils.trigger('log-warn', 'Lead Source Table is outdated, recommend resyncing with', chalk.bold.yellow('ils sync LeadSource'));

		}
		else
			debug('sync disabled, proceeding using remote data.');


		let lead_sources = await ils.query.LeadSource.execute();

		lead_sources.forEach(lead_source =>
		{
			lead_source_id_map[lead_source.Id];
			lead_source_name_map[lead_source.Name];
		});
	};

	let user_id_map    = Object.create(null);
	let user_email_map = Object.create(null);
	const updateUsers = async (is_cli) =>
	{
		if (ils.sync && !await ils.sync.isUpToDate('User'))
		{
			if (!is_cli)
			{
				ils.trigger('log-error', 'User Table is outdated, syncing now.');
				await ils.sync.sync('User');
			}
			else
				ils.trigger('log-warn', 'User Table is outdated, recommend resyncing with', chalk.bold.yellow('ils sync User'));

		}
		let users = await ils.query.User.execute();

		users.forEach(user => user_id_map[user.Id] = user);
		users.forEach(user => user_email_map[user.Email] = user);
	};

	let stage_id_map   = Object.create(null);
	let stage_name_map = Object.create(null);
	const updateStages = async (is_cli) =>
	{
		if (ils.sync && !await ils.sync.isUpToDate('Stage'))
		{
			if (!is_cli)
			{
				ils.trigger('log-error', 'Stage Table is outdated, syncing now.');
				await ils.sync.sync('Stage');
			}
			else
				ils.trigger('log-warn', 'Stage Table is outdated, recommend resyncing with', chalk.bold.yellow('ils sync Stage'));

		}
		else
			debug('sync disabled, proceeding using remote data.');


		let stages = await ils.query.Stage.execute();

		stages.forEach(stage =>
		{
			stage_id_map[stage.Id] = stage;
			stage_name_map[stage.StageName] = stage;
		});
	};

	const getUser = async user =>
	{
		let map;

		if (!isNaN(user) && (user % 1 === 0))
		{
			user = parseInt(user);
			map  = user_id_map;
		}
		else
			map = user_email_map;

		if (!map[user])
		{
			await updateUsers();
			if (!map[user])
					throw Error('User with id or email of ' + tag + ' does not exist in Infusionsoft.');
		}

		return map[user];
	};

	const getTag = async tag =>
	{
		let map;

		if (!isNaN(tag) && (tag % 1 === 0))
		{
			tag = parseInt(tag);
			map = tag_id_map;
		}
		else
			map = tag_name_map;

		if (!map[tag])
		{
			await updateTags();
			if (!map[tag])
					throw Error('Tag with name ' + tag + ' does not exist in Infusionsoft.');
		}

		return map[tag];
	}

	const getLeadSource = async leadsource =>
	{
		let map;

		if (!isNaN(leadsource) && (leadsource % 1 === 0))
		{
			leadsource = parseInt(leadsource);
			map = lead_source_id_map;
		}
		else
			map = lead_source_name_map;

		if (!map[leadsource])
		{
			await updateLeadSources();
			if (!map[leadsource])
				throw Error('LeadSource with name or ID ' + leadsource + ' does not exist in Infusionsoft.');
		}
		return map[leadsource];
	};

	const getStage = async stage =>
	{
		let map;

		if (!isNaN(stage) && (stage % 1 === 0))
		{
			stage = parseInt(stage);
			map = stage_id_map;
		}
		else
			map = stage_name_map;

		if (!map[stage])
		{
			await updateStages();
			if (!map[stage])
				throw Error('Stage with name or ID ' + stage + ' does not exist in Infusionsoft.');
		}
		return map[stage];
	};

	Object.defineProperty(self, 'updateCustomFields',
	{
		value      : updateCustomFields,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'updateUsers',
	{
		value      : updateUsers,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'updateTags',
	{
		value      : updateTags,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'updateLeadSources',
	{
		value      : updateLeadSources,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'updateStages',
	{
		value      : updateStages,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'getUser',
	{
		value      : getUser,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'getTag',
	{
		value      : getTag,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'getLeadSource',
	{
		value      : getLeadSource,
		enumerable : false,
		writable   : false,
	});

	Object.defineProperty(self, 'getStage',
	{
		value      : getStage,
		enumerable : false,
		writable   : false,
	});

	return self;
};

//For now, these are just strings, but I plan to replace the actual value with some
//kind of processing function.
const TYPES = 
{
	ID      : 'ID',
	NUMBER  : 'NUMBER',
	BOOLEAN : 'BOOLEAN',
	STRING  : 'STRING',
	DATE    : 'DATE',
	ARRAY   : 'ARRAY',
};

const INF_CF_TYPE_IDS = 
{
	1 : TYPES.STRING,  //Phone Number
	2 : TYPES.STRING,  //Social Security Number
	3 : TYPES.NUMBER,  //Currency
	4 : TYPES.NUMBER,  //Percent
	5 : TYPES.STRING,  //State
	6 : TYPES.BOOLEAN, //Yes/No
	7 : TYPES.DATE,    //Year
	8 : TYPES.STRING,  //Month
	9 : TYPES.STRING,  //Day of Week
	10: TYPES.STRING,  //Name	
	11: TYPES.NUMBER,  //Decimal Number	
	12: TYPES.NUMBER,  //Whole Number	
	13: TYPES.DATE,    //Date
	14: TYPES.DATE,    //Date/Time	
	15: TYPES.STRING,  //Text	
	16: TYPES.STRING,  //Text Area	
	17: TYPES.ARRAY,   //List Box
	18: TYPES.STRING,  //Website
	19: TYPES.STRING,  //Email
	20: TYPES.ARRAY,   //Radio
	21: TYPES.ARRAY,   //Dropdown
	22: TYPES.NUMBER,  //User
	23: TYPES.ARRAY,   //Drilldown
};

//takes a list of fields, returns a normalized version
const TABLE_NORMALIZE_FIELD_LIST = function (field_name_arr)
{
	return field_name_arr.map(name => this.fields_lc[name.toLowerCase()]);
};

