/**
 * API interface for infusionsoft-local-sync.
 * 
 * Provides access to the various infusionsoft services in as neat and normal a
 * way I can muster. 
 *
 * XMLRPC for now, when they get REST set up this system will convert.
 * TODO: Separate API calling functionality from schema management.
 */
'use strict';
const defs    = require('./defs');

module.exports = (Request, XmlRpc) => (ils, config) =>
{
	const debug_fn       = require('debug')('ils:api');
	const debug_fn_trace = require('debug')('(trace)ils:api');

	const debug = (...args) =>
	{
		debug_fn(...args);
		ils.trigger('log-debug', ...args);
	};
	const debug_trace = (...args) =>
	{
		debug_fn_trace(...args);
		ils.trigger('log-trace', ...args);
	};

	const self = Object.create(null);

	const app             = config.api.app;
	const key             = config.api.key;
	const retry_count     = config.api.retry_count || 5;
	const retry_interval  = config.api.retry_interval || 5;

	const getUserAPI = (access_token, refresh_token) =>
	{
		let oauth = access_token && refresh_token;
		if (oauth)
			var url = `https://api.infusionsoft.com/crm/xmlrpc/v1`;
		else
			var url = `https://${app}.infusionsoft.com/api/xmlrpc`;

		let xmlrpc = XmlRpc.createSecureClient(
			{
				url     : url,
				headers : {'Content-Type' : 'text/xml', 'X-Keap-API-Key' : access_token},
			});

		let uapi = Object.create(self);

		uapi.xmlrpc = (method_name, ...params) => new Promise((res, rej) =>
		{
			if (['getUserInfo'].indexOf(method_name) < 0)
				params = [oauth ? access_token : key, ...params];

			let fn_call = () =>
			{
				debug(`calling XML-RPC method: ${method_name}`, tried_count > 0 ? `try #${tried_count}` : '');
				debug_trace('---- with params:', JSON.stringify(params));
				xmlrpc.methodCall(method_name, params, fn_after);
			};
			let fn_after = (error, value) =>
			{
				if (!error)
					return res(value);

				//for Infusionsoft errors that actually make sense, we go ahead and reject immediately.
				/*if (error.message.indexOf('No method matching arguments') > -1)
					return rej(error);*/
				if (++tried_count >= retry_count)
				{
					ils.trigger('log-fatal', 'XML-RPC query failed after', tried_count, 'attempts with the message', error.message);
					return rej(error);
				}
				debug('XML-RPC failed, retrying ', tried_count, 'of', retry_count, 'times');
				debug_trace('XML-RPC error: ', error);

				ils.trigger('log-error', 'recoverable XML-RPC error (', error.message, ') retrying in', tried_count * retry_interval, 'seconds.');

				setTimeout(fn_call, tried_count * retry_interval * 1000);
				//fn_call();
			};
			let tried_count = 0;
			fn_call();
		});

		uapi.rest = (params, callback) =>
		{
			if (!oauth)
				throw Error('REST API requests can only be made via OAuth.');

			if ((typeof params) === 'string')
			{
				let uri        = params;
				params         = Object.create(null)
				params.uri     = uri;
				params.headers = Object.create(null);
			}
			else if (!params.headers)
				params.headers = Object.create(null);

			params.headers.Authorization = 'Bearer ' + access_token;
			params.headers.Accept        = 'application/json, */*';

			params.uri = 'https://api.infusionsoft.com/crm/rest/v1' + params.uri;

			/**
			 * I don't like forcing things on people either-- if you don't provide a callback
			 * I return a promise.
			 */
			let p = new Promise((res, rej) =>
				{
					let fn_call = () => Request(params, fn_after);

					let fn_after = (error, response, body) =>
					{
						if (!error)
							return res(body, response);

						debug('REST call failed, error code', response.code, 'try number:', tried_count);
						debug_trace('REST error:', error);

						if (++tried_count >= retry_count)
						{
							ils.trigger('log-fatal', 'REST query failed after', tried_count, 'attempts with the message', error.message);
							return rej(error);
						}

						ils.trigger('log-error', 'recoverable REST error (', error.message, ') retrying in', tried_count * 10, 'seconds.');
						setTimeout(tried_count * 10 * 1000, fn_call);
					};
					
					let tried_count = 0;
					fn_call();
				});
			if (!callback)
				return p
			else
				return p
						.then((body, response) => callback(undefined, response, body))
						.catch(err=>callback(err));
		};

		return uapi;
	};

	self.getUserAPI = getUserAPI;

	return getUserAPI();
};

/**
 * The Proxy creation syntax is really horrible.  So is `new Promise`, but it 
 * doesn't add to the pyramid so it's OK. 
 */
const mk_proxy = (obj, fn_get, fn_set) =>
{
    let conf = {};
    if (fn_get)
        conf.get = fn_get;
    if (fn_set)
        conf.set = fn_set;
    return new Proxy(obj, conf);
}
