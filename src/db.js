/**
 * Sets up our local interface to the database.
 */
'use strict';
const debug       = require('debug')('ils:db');
const knex        = require('knex');
const join        = require('path').join;
const fs          = require('fs');
const file_exists = fs.existsSync;

module.exports = knex => (ils, config) => 
{
	let c = Object.assign(Object.create(null), config.sync.db_client);
	let db = knex(c);

	db.runMigrations = async force =>
	{
		await db.raw(`CREATE SCHEMA IF NOT EXISTS ${config.sync.schema};`);
		let exists  = db.schema.hasTable('ils_migrations');
		let already = db('ils_migrations').where('name', '=', '00-intial.js').count().first();
		
		if (await exists && parseInt((await already).count) > 0)
			throw Error('It appears the database is already installed.');

		if (!force && file_exists(join(process.cwd(), 'knexfile.js')))
			throw Error('It looks like you\'re using knex.js to manage your database migations. You may want to use `ils db make-knex-migrations` instead. Use --force to override this message.');
		return await db.migrate.latest(
		{
			directory : join(__dirname, 'migrations'),
			tableName : 'ils_migrations',
		});
	};

	db.rollbackMigrations = async force =>
	{
		let exists  = db.schema.hasTable('ils_migrations')
		let already = db('ils_migrations').where('name', '=', '00-intial.js').count().first();

		if (!(await exists) || parseInt((await already).count) < 1)
			throw Error('It does not appear the ILS database is installed. Check your configuration.');

		if (!force && file_exists(join(process.cwd(), 'knexfile.js')))
			throw Error('It looks like you\'re using knex.js to manage your database migations. You may want to use `ils db make-knex-migrations` instead. Use --force to override this message.');
		return await db.migrate.rollback(
		{
			directory : join(__dirname, 'migrations'),
			tableName : 'ils_migrations',
		});
	};

	db.makeKnexMigrations = (knexconf) =>
	{
		let mig_dir = knexconf.migrations && knexconf.migrations.directory|| join(process.cwd(), 'migrations');
		let mig_file = join(mig_dir, new Date().toISOString().replace(/[-:]|\..*/g, '') + '-infusionsoft-local-sync.js');
		return new Promise((res, rej) =>
		{
			let src = fs.createReadStream(join(__dirname, '../res/knex_migration.js'))
			let dst = fs.createWriteStream(mig_file);

			src.on('end', () =>
			{
				res(mig_file);
				src.destroy();
			})
			.on('error', rej);

			dst.on('end', () =>
			{
				res(mig_file);
				dst.destroy()
			})
			.on('error', rej);

			src.pipe(dst);
		});
	};

	ils.listen('close', () => db.destroy());
	return db;
};

