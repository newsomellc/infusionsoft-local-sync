module.exports = fail =>
{
	let self = {};

	self.methodCall = (...params) => 
	{
		if (fail)
			params[2](fail, 0);
		else
			params[2](undefined, 0);
		//else NOP
	};
	self.createSecureClient = () =>
	{
		return self;
	};
	return self;
};

