'use strict';
/** 
 * A standardized test template for ILS.
 *  
 * Pass it either the real ILS or the mock ILS core, and you'll get back 
 * an instance of whatever you passed along with a list of fake contacts, opportunitites,
 * and whatever else I decide to add later.
 */
const Core  = require('../../src/core');
const gmfcs = require('./give-me-fake-contacts');
const gmfls = require('./give-me-fake-leads');

module.exports = async (ILS, contact_count=0, lead_count=0) =>
{
	let ils = ILS('', 'NO_SYNC');

	await ils._db.rollbackMigrations().catch(()=>{});
	await ils._db.runMigrations();

	let ps =
	[
		ils.schema.updateCustomFields(false),
		ils.schema.updateLeadSources(false),
		ils.schema.updateTags(false),
		ils.schema.updateUsers(false),
	];

	await Promise.all(ps);
	await ils.ready();

	let contacts    = gmfcs(contact_count);
	let contact_ids = await ils.q.contact.insert(contacts);
	contacts        = await ils.q.contact.whereIn('Id', contact_ids).execute();

	let leads       = gmfls(lead_count, contacts);
	let lead_ids    = await ils.q.lead.insert(leads);
	leads           = await ils.q.lead.whereIn('Id', lead_ids).execute();

	let close_res;
	let closed = new Promise(res => close_res = res);
	ils.listen('close', async () =>
	{
		await Promise.all(leads.map(    l => ils.api.xmlrpc('DataService.delete', 'Lead',    l.Id)));
		await Promise.all(contacts.map( c => ils.api.xmlrpc('DataService.delete', 'Contact', c.Id)));
		close_res();
	});

	return {ils:ils, contacts:contacts, leads:leads, closed:closed};
};
