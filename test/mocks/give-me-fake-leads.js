const FNAMES   = ['Imelda','Kimbra','Daine','Kimi','Reyes','Shonda','Romeo','Mitsue','Theresa','Machelle','Dorthy','Janetta','Caitlyn','Alaine','Randa','Gregoria','Jen','Terence','Alanna','Maegan','Marylin','Florentina','Rima','Floria','Nigel','Chas','Renea','Michaele','Trena','Annalee','Candyce','Diedre','Thao','Delmar','Loree','Linn','Jodee','Cyrstal','Zack','Sharell','Rochell','Bette','Evelynn','Scottie','Manuel','Marvin','Maple','Erica','Nereida','Darcie'];
const LNAMES   = ['Stickland','Pittmon','Mastro','Owen','Marty','Nuckols','Nath','Levert','Pooser','Leer','Nevitt','Ross','Zufelt','Barner','Haislip','Tucci','Mathis','Martinsen','Turberville','Beckham','Tugwell','Geesey','Bagwell','Newborn','Fells','Brenner','Elwood','Visser','Erskine','Ferrigno','Tisby','Grieves','Rybicki','Waite','Mullins','Whitwell','Stanfield','Spratling','Revis','Ortiz','Montalvan','Hixon','Lease','Cabrales','Burget','Bendickson','Argo','Corder','Tammaro','Rambo'];
const CNAMES   = ['3Com Corp', '3M Company', 'A.G. Edwards Inc.', 'Abbott Laboratories', 'Abercrombie & Fitch Co.', 'ABM Industries Incorporated', 'Ace Hardware Corporation', 'ACT Manufacturing Inc.', 'Acterna Corp.', 'Adams Resources & Energy, Inc.', 'ADC Telecommunications, Inc.', 'Adelphia Communications Corporation', 'Administaff, Inc.', 'Adobe Systems Incorporated', 'Adolph Coors Company', 'Advance Auto Parts, Inc.', 'Advanced Micro Devices, Inc.', 'AdvancePCS, Inc.', 'Advantica Restaurant Group, Inc.', 'The AES Corporation', 'Aetna Inc.', 'Affiliated Computer Services, Inc.', 'AFLAC Incorporated', 'AGCO Corporation', 'Agilent Technologies, Inc.', 'Agway Inc.', 'Apartment Investment and Management Company', 'Air Products and Chemicals, Inc.', 'Airborne, Inc.', 'Airgas, Inc.', 'AK Steel Holding Corporation', 'Alaska Air Group, Inc.', 'Alberto-Culver Company', 'Albertson\'s, Inc.', 'Alcoa Inc.', 'Alleghany Corporation', 'Allegheny Energy, Inc.', 'Allegheny Technologies Incorporated', 'Allergan, Inc.', 'ALLETE, Inc.', 'Alliant Energy Corporation', 'Allied Waste Industries, Inc.', 'Allmerica Financial Corporation', 'The Allstate Corporation', 'ALLTEL Corporation', 'The Alpine Group, Inc.', 'Amazon.com, Inc.', 'AMC Entertainment Inc.', 'American Power Conversion Corporation', 'Amerada Hess Corporation', 'AMERCO', 'Ameren Corporation', 'America West Holdings Corporation', 'American Axle & Manufacturing Holdings, Inc.', 'American Eagle Outfitters, Inc.', 'American Electric Power Company, Inc.', 'American Express Company', 'American Financial Group, Inc.', 'American Greetings Corporation', 'American International Group, Inc.', 'American Standard Companies Inc.', 'American Water Works Company, Inc.', 'AmerisourceBergen Corporation', 'Ames Department Stores, Inc.', 'Amgen Inc.', 'Amkor Technology, Inc.', 'AMR Corporation', 'AmSouth Bancorp.', 'Amtran, Inc.', 'Anadarko Petroleum Corporation', 'Analog Devices, Inc.'];
const SUBJECTS = [...FNAMES, ...LNAMES, ...CNAMES,];
const VERBS    = ['switched', 'connected', 'zoomed', 'sailed', 'satisfied', 'hated', 'bated', 'rhymed', 'locked', 'pretended', 'talked', 'polished', 'dared', 'reached', 'followed', 'groaned', 'rescued', 'hugged', 'bleached', 'formed', 'influenced', 'slipped', 'reminded', 'glowed', 'preached', 'deserted', 'strapped', 'replied', 'frightened', 'parted', 'cheered', 'gazed', 'argued', 'presented', 'coughed', 'returned', 'helped', 'punished', 'surprised', 'instructed', 'griped', 'knocked', 'improved', 'crushed', 'played', 'mined', 'whistled', 'booked', 'objected'];
const OBJECTS  = ['cannon','invention','approval','care','wing','space','rice','top','children','rest','toothpaste','attraction','vessel','pigs','move','spy','twig','wall','peace','horn','earth','door','statement','stamp','achiever','sea','cactus','ray','eyes','hydrant','cart','power','jam','bell','bulb','hot','thread','cattle','expert','pear','request','camera','design','quill','flesh','reaction','treatment','guide','story','whistle'];

module.exports = (count, contacts) =>
{
	if (!contacts)
		throw Error('need contacts to link leads to.');

	let ls = [];
	let counter = 0;
	while(counter < count)
	{
		let c = contacts[counter % contacts.length];
		counter++;
		let l = 
		{
			ContactID            : c.Id,
			OpportunityTitle     : rstring(10 + Math.floor(Math.random() * 30)),
			OpportunityNotes     : rstring(100 + Math.floor(Math.random() * 250)),
			ProjectedRevenueHigh : Math.random() * 100,
			ProjectedRevenueLow  : Math.random() * 100,
		};
		l.ProjectedRevenueHigh += l.ProjectedRevenueLow;
		ls.push(l);
	}
	return ls;
};

const CHRS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890!@#$%^&*()-=_+[];\',./\/{}|:"<>?';
rstring = (len) =>
{
	let counter = 0;
	let str = '';
	while (len > counter)
	{
		let cidx =  Math.floor(Math.random() * CHRS.length);
		str += CHRS[cidx];
		counter++;
	}
	return str;
};
