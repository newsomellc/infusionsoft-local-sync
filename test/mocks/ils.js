module.exports = () =>
{
	let self = {};

	self.trigger = () => {};
	self.listen  = () => {};

	return self;
}