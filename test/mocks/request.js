module.exports = fail => () =>
{
	let self = (params, callback) => 
	{
		if (callback)
			return callback(fail);

		return Promise.reject(fail);

		//else NOP
	};
	return self;
};
