'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

const ILS        = require('../src');
const ILSMock    = require('./mocks/ils');
const setup_case = require('./mocks/setup-case');

describe('ils:core', () =>
{
	it('should instantiate without errors', async function ()
	{
		this.timeout(5000);
		let ils = ILS();

		await ils.ready();
		await ils.close();
	});
	/**/

	it('should return a promise from its "ready" method that resolves after async init tasks.', async function ()
	{
		this.timeout(5000);
		let {ils, closed} = setup_case(ILS);

		console.log(ils);

		expect(ils.ready()).to.eventually.equal(undefined);
		ils.close();
		await closed;
	});

});

