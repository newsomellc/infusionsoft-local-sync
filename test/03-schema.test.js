'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));
chai.use(require('chai-spies'));

const ILS        = require('../src');
const ILSMock    = require('./mocks/ils');
const setup_case = require('./mocks/setup-case');

describe('ils:schema', () =>
{
	/**/
	it('should build an internal structure of Infusionsoft fields etc');

	/**/
	it('should link tables to their aliases');

	/**/
	it('should keep a list of Users (table: User)');

	/**/
	it('should keep a list of Tags (table: ContactGroup)');

	/**/
	it('should keep a list of custom fields (table: DataFormField)');

	/**/
	it('should keep a list of lead sources (table: LeadSource)');

	/**/
	it('should add custom fields to its internal structure when updateCustomFields is called');

	/**/
	it('should properly resolve a tag to its tag ID from cache by name');

	/**/
	it('should properly resolve a tag to its tag ID by ID (to verifying it exists)');

	/**/
	it('should throw an error if a request is made for a nonexistent tag');

	/**/
	it('should update tag list on a query for a nonexistent tag before erroring');

	/**/
	it('should properly resolve a field to its name from cache by label');

	/**/
	it('should properly resolve a User from cache by its ID');

	/**/
	it('should properly resolve a User from cache by its Email');

	/**/
	it('should update Users if a request for a (locally) nonexistent user is made');

	/**/
	it('should normalize capitalization when given a list of fields');

	/**/
	it('should attempt to resolve labels to field names from a list if prefixed with a colon');

	/**/
	it('should warn when custom field list is out of date when invoked via CLI.');

	/**/
	it('should just go ahead and sync itself when invoked as a dependency');

	/* can't really test this yet *
	it('should update changes to custom fields in its internal structure when updateCustomFields is called', function ()
	{
		throw Error('test not written');
	});

	/**/

});
