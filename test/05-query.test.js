'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

const ILS        = require('../src');
const ILSMock    = require('./mocks/ils');
const setup_case = require('./mocks/setup-case');

describe('ils:query', () =>
{
	/**/
	it('should automatically choose remote when a local database isn\'t configured or available');

	/**/
	it('should retrieve objects using equals (=) or LIKE queries remotely');

	/**/
	it('should retrieve contacts by tag');

	/**/
	it('should automatically source from local or remote depending on the query');

	/**/
});

describe('ils:query:remote', () =>
{
	/**/
	it('should create, retrieve, and delete objects');

	/**/
	it('should tag and untag contacts');

	/* This one is WAY more complicated than it should be. */
	it('should retrieve contacts by tagged and not tagged');

	/**/
	it('should fail triggering a helpful error message if you attempt an incompatible query type');

	/**/
});

describe('ils:query:local', () =>
{
	/**/
	it('should retrieve from the local database');

	/**/
	it('should get contacts by tag');

	/**/
	it('should be able to select using most knex methods (whereIn, orWhere, whereBetween, etc)');

	/**/
	it('should resolve joins when requested');

	/**/
	it('should properly warn if a query is attempted against a table that is out of date (as defined, in minutes, by config:sync:interval');

	/**/
	it('should NOT warn after a previously stale table is synced');

	/**/
});
