'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));
chai.use(require('chai-spies'));

const ILS         = require('../src');
const ILSMock     = require('./mocks/ils');
const XmlRpcMock  = require('./mocks/xmlrpc');
const RequestMock = require('./mocks/request');
const setup_case  = require('./mocks/setup-case');
const gmfcs       = require('./mocks/give-me-fake-contacts');

describe('ils:api', () =>
{
	/**/
	it('should allow basic CRUD operations on Contacts', async function ()
	{
		this.timeout(5000);
		
		let {ils, closed} = await setup_case(ILS, 0, 0);
		let test_contacts = gmfcs(3);

		let add_contacts = Promise.all(test_contacts.map(c => ils.api.xmlrpc('ContactService.add', c)));
		
		await expect(add_contacts).to.eventually.be.an('array').with.lengthOf(3)
			.satisfies(nums => nums.every(num => !isNaN(num)));

		let contact_ids = await add_contacts;

		let load_contacts = Promise.all(contact_ids.map(c => ils.api.xmlrpc('ContactService.load', c, ils.schema.Contact.fields)));


		await expect(load_contacts).to.eventually.be.an('array').with.lengthOf(3)
			.satisfies(contacts => contacts.every(contact => (typeof contact.Id) === 'number'), 'loaded contacts do not have IDs')
			//.satisfies(contacts => contacts.every(contact => expect(test_contacts).to.contain(contact)));

		let contacts = await load_contacts;

		let data_service_queries = Promise.all(test_contacts.map(async c =>
		{
			let data_service_query = ils.api.xmlrpc('DataService.query', 'Contact', 1, 0, c, ils.schema.Contact.fields, 'Id', true);
			expect(data_service_query).to.eventually.be.an('array');
			return data_service_query.then(res => res[0]);
		}));

		let queried_contacts = await data_service_queries;

		let delete_contacts = Promise.all(queried_contacts.map(qc => ils.api.xmlrpc('DataService.delete', 'Contact', qc.Id)));

		await expect(delete_contacts).to.eventually.be.an('array').with.lengthOf(3)
			.satisfies(rs => rs.every(r => r));

		let del_results = await delete_contacts;

		ils.close();

		await closed;

	});

	/**/
	it('should allow CRUD operations on custom fields transparently-- without needing to explicitly add them', async function ()
	{
		this.timeout(5000);
		let {ils, contacts, closed} = await setup_case(ILS, 3, 0);

		let find_field = ils.api.xmlrpc(
				'DataService.query',
				'DataFormField', 1, 0, {Label: 'Custom Text', FormID: ils.schema.Contact.data_form_id}, ils.schema.DataFormField.fields,'Id', true);

		expect(find_field).to.eventually.be.an('array').length.greaterThan(0).satisfies(result => result[0].Label === 'Custom Text');

		let fname = '_' + (await find_field).Name;


		let tests = contacts.map(async contact =>
		{
			let update = ils.api.xmlrpc('DataService.update', 'Contact', contact.Id, {[fname]: 'My Text'});
			expect(update).to.eventually.equal(contact.Id);
			await update;

			let query = ils.api.xmlrpc('DataService.load', contact.Id, ils.schema.Contact.fields);

			let rescontact = await query;

			expect(rescontact).to.contain('Id').equals(contact.Id);
			expect(rescontact).to.contain(fname).equals('My Text');
		});

		await Promise.all(tests);

		ils.close();
		await closed;
	});
	/**/
	it('should retry failed queries the amount of times specified in config (api:retry_count)', async function ()
	{
		let ourerr = Error('some bogus error');

		let ils = ILSMock();

		let mr = RequestMock(ourerr);
		let mx = XmlRpcMock(ourerr);

		let api = require('../src/api')
			(mr, mx)
			(ils, {api: {retry_count:5, retry_interval:.01}});

		//test REST
		/* Oh yeah, that's right, REST throws an error unless called via OAuth.  No CLI test suite yet! *
		mr.methodCall = chai.spy(mr.methodCall);

		await expect(api.rest('/not/a/real/thing/')).to.be.rejectedWith(ourerr);
		await expect(mx.methodCall).to.have.been.called.exactly(5);

		//test both styles!
		await expect(new Promise((res, rej) => 
		{
			let callback = (err, val)
			{
				if (err)
					rej(err);
				else
					res(val);
			};
			api.rest('/not/a/real/thing/', callback);
		})).to.be.rejectedWith(ourerr);;

		await expect(mx.methodCall).to.have.been.called.exactly(10);
		/**/
		//test XML-RPC
		mx.methodCall = chai.spy(mx.methodCall);

		await expect(api.xmlrpc('NoService.whatever', 'p1', 'p2')).to.be.rejectedWith(ourerr);
		await expect(mx.methodCall).to.have.been.called.exactly(5);
	});
	/**/
});
