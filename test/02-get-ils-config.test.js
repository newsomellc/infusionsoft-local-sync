'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

const get_ils_config = require('../src/get-ils-config');

describe('ils:get-ils-config', () =>
{
	if (require('fs').existsSync('infusionsoft-local-sync.json') || require('fs').existsSync('infusionsoft-local-sync.development.json'))
	{
		it('skipped all config tets-- infusionsoft-local-sync.json or variant would be overwritten.');
		return;
	}
	const cfn = (...path) => require('path').join(__dirname, 'test-configs', ...path);

	const mk_params = (i, e, p, h,s) =>
	{
		let params = 
		{
			ilsfile : i ? cfn(i===true?'custom-conf-file.json':i) : undefined,
			env     : e === true ? 'env' : (e === false ? 'nonexistent' : e),
			cwd     : p || e ? cfn() : cfn('nonexistent'),
			homedir : h ? cfn('homedir') : cfn('nonexistent'),
			sysdir  : s ? cfn('sysdir') : cfn('nonexistent'),
		};
		return params;
	};

	const run_tests_for_whats_loaded = (conf, i, e, p, h, s) =>
	{
		let test = (name, shouldaloaded) => 
		{
			if (shouldaloaded === 'ignore')
				return;

			if (shouldaloaded)
				return expect(conf.get(name+'_conf_loaded')).to.equal(true, `conf ${name} failed to load`)
			else
				return expect(conf.get(name+'_conf_loaded')).to.equal(undefined, `conf ${name}' loaded and it shouldn't have`);
		};

		[['custom', i], ['project', p], ['user', h], ['system', s]].forEach(cf => cf ? test(...cf) : '');

		if (e === true)
			test('env', true);
		else if (e === false)
			test('env', undefined);
		else if (e === undefined && p)
			test('development', true);
	};


	/**/
	it('should load basic configs without raising errors', () =>
	{
		let p = [false, true, true, true, true];
		run_tests_for_whats_loaded(get_ils_config(mk_params(...p)), ...p);
	});

	/**/
	it('should load all config files', () =>
	{
		let p = [true, true, true, true, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);
	});

	/**/
	it('should load config file via the --ilsfile param', () =>
	{
		let p = [true, false, false, false, false];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);
	});

	/**/
	it('should load config file from the user\'s home directory', () =>
	{
		let p = [false, false, false, true, false];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);
	});

	/**/
	it('should load config file from the system directory', () =>
	{
		let p = [false, false, false, false, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);
	});

	/**/
	it('should treat the ilsfile parameter as the highest priority config', () =>
	{
		let p = [true, true, true, true, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('overridden_key')).to.equal('overridden by custom conf');
	});

	/**/
	it('should treat the project environment as the next highest priority config', () =>
	{
		let p = [false, true, true, true, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('overridden_key')).to.equal('overridden by environment conf');
	});

	/**/
	it('should treat the project(cwd) as the next highest priority config', () =>
	{
		let p = [false, false, true, true, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('overridden_key')).to.equal('overridden by project conf');
	});

	/**/
	it('should treat the user home config as the next highest priority config', () =>
	{
		let p = [false, false, false, true, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('user_override')).to.equal('should have this text if user config is used');
		expect(conf.get('overridden_key')).to.equal('overridden by user conf');
	});

	/**/
	it('should treat system as the lowest priority config', () =>
	{
		let p = [false, false, false, false, true];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('user_override')).to.equal('should only have this text if user config isn\'t used');
		expect(conf.get('overridden_key')).to.equal('base system text-- only exists if nothing overrides this');
	});

	/**/
	it('should load a config file based on the NODE_ENV variable', () =>
	{
		let p = [false, 'development', true, false, false];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('development_conf_loaded')).to.equal(true);
		expect(conf.get('overridden_key')).to.equal('development conf-- loaded by default if it exists and NODE_ENV is not set.');
	});

	/**/
	it('should load infusionsoft-local-sync.development.json in the current project by default (if it exists)', () =>
	{
		let p = [false, undefined, true, false, false];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('development_conf_loaded')).to.equal(true);
		expect(conf.get('overridden_key')).to.equal('development conf-- loaded by default if it exists and NODE_ENV is not set.');
	});

	/**/
	it('should not load infusionsoft-local-sync.development.json if NODE_ENV is something other than falsy or `development`.', () =>
	{
		let p = [false, 'production', true, false, false];
		let conf = get_ils_config(mk_params(...p));
		run_tests_for_whats_loaded(conf, ...p);

		expect(conf.get('development_conf_loaded')).to.equal(undefined);
		expect(conf.get('overridden_key')).to.equal('overridden by project conf');
	});

	/**/
	it('should load a config file given a path param, and it should be exclusive.', () =>
	{
		let p = cfn('custom-conf-file.json');
		let conf = get_ils_config(p);
		run_tests_for_whats_loaded(conf, true, false, false, false, false);

		expect(conf.get('overridden_key')).to.equal('overridden by custom conf');
	});

	/**/
	it('should load based on CWD and environment variables if no params are provided', function ()
	{
		if (process.env.NODE_ENV !== undefined && process.env.NODE_ENV !== 'development')
			this.skip('NODE_ENV is not "development"');

		const cp    = require('fs-copy-file-sync');
		const pjoin = require('path').join;
		const rm    = require('fs').unlinkSync;

		//have to rename development to env for run_tests_for_whats_loaded to work, since the unchangeable default is nothing.
		let dstpthd = pjoin(process.cwd(), 'infusionsoft-local-sync.development.json');
		let dstpth = pjoin(process.cwd(), 'infusionsoft-local-sync.json');
		cp(pjoin(__dirname, 'test-configs', 'infusionsoft-local-sync.env.json'), dstpthd);
		cp(pjoin(__dirname, 'test-configs', 'infusionsoft-local-sync.json'), dstpth);

		let conf = get_ils_config();

		rm(dstpth);
		rm(dstpthd);

		run_tests_for_whats_loaded(conf, false, true, true, 'ignore', 'ignore');
		expect(conf.get('custom_override')).to.equal('should only have this text if custom config is NOT used');
		expect(conf.get('project_override')).to.equal('should have this text if project config is used');
	});

	/**/
	it('should throw an error if given a nonexistent ilsfile param', () =>
	{
		let p = [false, false, false, false, false];
		let prms = mk_params(...p);
		prms['ilsfile'] = require('path').join(__dirname, 'test-configs', 'nonexistent file');

		expect(()=>get_ils_config(prms)).to.throw('Given config file doesn\'t exist');
	});

	/**/
	it('should throw an error if config lacks api:app param', () =>
	{
		let p = [false, false, false, false, false];
		let prms = mk_params(...p);

		prms['ilsfile'] = require('path').join(__dirname, 'test-configs', 'custom-conf-file-lacks-api.json');

		expect(()=>get_ils_config(prms)).to.throw('ILS is not configured or no config loaded.');
	});

	/* this will also apply to any of the others, but the problem was that it was STILL loading CWD despite it not going anywhere. */
	it('should not load cwd config file if its params value is specifically set to false', () =>
	{
		const cp    = require('fs-copy-file-sync');
		const pjoin = require('path').join;
		const rm    = require('fs').unlinkSync;

		let p = [false, false, false, true, true];
		let prms = mk_params(...p);
		prms['cwd'] = false;

		let dstpth = pjoin(process.cwd(), 'infusionsoft-local-sync.json');
		cp(pjoin(__dirname, 'test-configs', 'infusionsoft-local-sync.json'), dstpth);

		let conf = get_ils_config(prms);
		rm(dstpth);

		expect(conf.get('overridden_key')).to.equal('overridden by project conf');
	});

	/**/

});
