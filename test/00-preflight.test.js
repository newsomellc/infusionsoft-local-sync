'use strict';
const chai   = require('chai');
const expect = chai.expect;
chai.use(require('chai-as-promised'));

const ILS        = require('../src');
const ILSMock    = require('./mocks/ils');
const setup_case = require('./mocks/setup-case');

describe('ils:preflight', () =>
{

	/**/
	it('you absolutely should not run these tests on your production account-- to be sure, delete all Contacts, Opportunities or Products from it', async function ()
	{
		this.timeout(10000);
		let {ils, closed} = await setup_case(ILS);

		await ils.ready();


		let [ccnt, lcnt, pcnt] = await Promise.all(
		[
			ils.q.contact.remote().count(),
			ils.q.lead.remote().count(),
			ils.q.product.remote().count(),
		]);

		if (parseInt(ccnt || lcnt || pcnt))
		{
			console.log(`Found the following in your database-- Contacts: ${ccnt} Leads/Opportunities: ${lcnt} Products: ${pcnt}. Please empty your Infusionsofft app of all contacts, leads, or products before running.`);
			ils.close();
			await closed;
			process.exit(1);
		}

		ils.close();
		await closed;
	});

	/**/
	it('you should have a tag named "Unit Testing" on your Infusionsoft app');

	/**/
	it('you should have a custom Text field named "Custom Text" for Contacts on your Infusionsoft app');

	/**/
	it('you should have a custom Text field named "Custom Text" for Leads(Opportunities) on your Infusionsoft app');

	/**/
	it('you should have a custom DateTime field named "Custom Date/Time" for Contacts on your Infusionsoft app');

	/**/
	it('you should have a custom DateTime field named "Custom Date/Time" for Leads(Opportunities) on your Infusionsoft app');

	/**/
	it('you should have a custom Yes/No field named "Custom Boolean" for Contacts on your Infusionsoft app');

	/**/
	it('you should have a custom Yes/No field named "Custom Boolean" for Leads(Opportunities) on your Infusionsoft app');

	/**/
	it('you should have a custom MultiSelect field named "Custom MultiSelect" for Contacts on your Infusionsoft app');

	/**/
	it('you should have a custom MultiSelect field named "Custom MultiSelect" for Leads(Opportunities) on your Infusionsoft app');

	/**/
});
