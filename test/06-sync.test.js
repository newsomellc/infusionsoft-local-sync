'use strict';
const chai   = require('chai');
const expect = chai.expect;

chai.use(require('chai-as-promised'));

const InfLocSyn  = require('../src/index');
const setup_case = require('./mocks/setup-case');

describe('ils:sync', () =>
{
	const inf = InfLocSyn();

	it('should import Contacts to the local database');

	it('should import Leads(opportunities) to the local database');

	it('should import ContactActions (notes, tasks, etc) to the local database');

	it('should import ContactGroups (tags) to the local database');

	it('should import ContactGroupAssigns (table that maps tags to the contacts that are assigned them) to the local database');

	it('should import LeadSources to the local database');

	it('should refuse to start if _syncmeta table reports that sync is already running');

	it('should always do complete imports on tables that can\'t be reliably synced any other way');

	it('should do incremental syncs on Id-based tables');

	it('should do incremental syncs on Date-based tables');

	it('should clean ContactGroupAssigns after syncing the Contact table (to remove tag assignments that were removed)');
	
	it('should emit a progress event on progress (after every write to the database)');
	
	it('should emit a finished event when done');

	it('should update syncmeta between each page with current page');
});
