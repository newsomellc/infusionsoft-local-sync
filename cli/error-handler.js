/** 
 * Error handler.
 */
const chalk = require('chalk');
module.exports = cb => 
{
	let e = error =>
	{
		console.error(chalk.bold.red('Error:'), chalk.red(error.message));
		if (process.env.NODE_ENV === 'development' || !process.env.NODE_ENV)
		{
			console.error(chalk.bold.magenta('Stack trace follows:'));
			console.error(chalk.bold(error.stack));
		}
		if (cb) 
			cb();
		process.exit(1);
	};

	return e;
};
