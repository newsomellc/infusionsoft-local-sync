#!/usr/bin/env node
/**
 * CLI sync verb.
 */
'use strict';
const debug = require('debug')('ils:cli:sync');
const chalk   = require('chalk');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = (program) =>
{
	let command = program
		.command('sync <table>')
		.alias('s')
		.option('-c, --ilsfile <config file>', 'run with specified ILS file')
		.option('-f, --force', 'start running despite mutex lock')
		.option('-t, --truncate', 'clear local database and re-sync from beginning')
		.option('-p, --prune', 'clear deleted remote entries from the local database after sync')
		.option('--just-prune', 'ONLY clear deleted remote entries from the local database, don\'t sync')
		.option('-o, --out [output format]','specify report format (table, json or csv)', 'table')
		.description('Load items from Infusionsoft into the local database.');
	command.action(action(command));
	return command;
};
const action = command => async table =>
{
	let ils = ILS(command.ilsfile, 'NO_SYNC');
	require('./loggers')(ils);

	ils.trigger('log-begin', '----', chalk.bold.blue(require('moment')().format('YYYY-MM-DD hh:mm:ss')), '----');

	let error = false; 
	let errhandlr = ErrorHandler(() => 
	{
		error = true;
		ils.close();
	});
	//TODO: have some sort of external error handler-- this is nutso
	if (!ils.schema[table])
		errhandlr(Error(`Unknown table or alias: ${table}`));

	table = ils.schema[table].canonical_name;

	if (!command.justPrune)
	{
		if (ils.schema[table].has_custom_fields && !await ils.sync.isUpToDate('DataFormField'))
		{
			ils.trigger('log-error', 'Custom Fields (DataFormField) table is not up to date-- syncing now.');
			await ils.sync.sync('DataFormField', false, command.force).catch(errhandlr);
		}

		if (error)
			return ils.close();

		await ils.sync.sync(table, command.truncate, command.force).catch(errhandlr);
	}

	if (command.prune || command.justPrune)
		await ils.sync.prune(table, command.force);

	ils.close();
};
