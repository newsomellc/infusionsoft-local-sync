#!/usr/bin/env node
/**
 * CLI sync verb.
 */
'use strict';
const fs      = require('fs');
const debug   = require('debug')('ils:cli:db');
const chalk   = require('chalk');
const join    = require('path').join;
const dirname = require('path').dirname;

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = program =>
{
	let command = program
		.command('db <subcommand>')
		.option('-c, --ilsfile <config file>', 'Run with specified ILS file')
		.option('-k, --knexfile <config file>', 'Run with specified knexfile')
		.option('-f, --force', 'force installation even though a local knexfile was detected');

	command.action(action(command));
};

const action = (command) => verb => 
{
	let ils = ILS(command.ilsfile, 'NO_SYNC');
	let errhand = ErrorHandler(() => ils.trigger('close'));
	try
	{
		switch (verb)
		{
			case 'install':
				ils._db.runMigrations(command.force).then(r =>
				{
					console.log(chalk.green('Database setup complete.'));
					ils.trigger('close');
				}).catch(errhand);
				break;
			case 'uninstall':
				ils._db.rollbackMigrations(command.force).then(r =>
				{
					console.log(chalk.green(`Database uninstalled.  Empty schema '${ils._config.sync.schema}' may remain-- remove at your convenience.`));
					ils.trigger('close');
				}).catch(errhand);
				break;
			case 'make-knex-migrations':
				let knfpath = join(process.cwd(), command.knexfile || './knexfile.js');
				var knexfile = require(knfpath);
				if (!fs.existsSync(knfpath))
					throw Error(`Could not find knexfile at ${knfpath}. Specify it with the '--knexfile=<path>' option.`);
				
				let env = process.env.NODE_ENV ||'development';
				if (!knexfile[env])
					throw Error('No knex configuration for current nodejs environment.');
				if (!knexfile[env].migrations)
					knexfile[env].migrations = {};
				if (!knexfile[env].migrations.directory)
					knexfile[env].migrations.directory = join(dirname(command.knexfile), 'migrations');

				ils._db.makeKnexMigrations(knexfile[env])
					.then(mig_file =>
					{
						console.log(chalk.bold.green('wrote knex migration file: '), mig_file);
						ils.trigger('close');
					})
					.catch(errhand);
				break;
			default:
				throw Error('Unknown command: ' + verb + '. Valid subcommands: install, uninstall, make-knex-migrations');
				ils.trigger('close');
				break;
		}
	}
	catch (ex)
	{
		return errhand(ex);
	}

};