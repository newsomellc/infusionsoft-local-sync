/**
 * Attaches logger functions-- log when certain runtime errors/warnings notices occur.
 * Useful for CLI.
 */
const chalk=require('chalk');
module.exports = (ils, debug) =>
{
	let rlog, clog, elog;

	if (ils._config.cli && ils._config.cli.recoverable_to_stderr)
		rlog = console.error;
	else
		rlog = console.log;

	clog = console.log;
	elog = console.error;

	//just for completeness's sake, may not use it.
	if (debug)
	{
		ils.listen('log-trace', (...args) => debug(chalk.blue.dim( '    >' ), args.join(' ')));
		ils.listen('log-debug', (...args) => debug(chalk.blue(     '    >' ), args.join(' ')));
	}
	ils.listen('log-begin',    (...args) => clog(chalk.bold.green(     '--->'   ), chalk.green(args.join(' '))));
	ils.listen('log-success',  (...args) => clog(chalk.bold.green(     '>>>>'   ), chalk.green(args.join(' '))));
	ils.listen('log-notice',   (...args) => clog(chalk.bold.cyan(      '  ->' ), args.join(' ')));
	ils.listen('log-info',     (...args) => clog(chalk.blue(           '   *' ), args.join(' ')));
	ils.listen('log-error',    (...args) => rlog(chalk.bold.yellow(    '  !>' ), args.join(' ')));
	ils.listen('log-warn',     (...args) => elog(chalk.rgb(200,100,0)( ' !!>' ), args.join(' ')));
	ils.listen('log-fatal',    (...args) => elog(chalk.bold.red(       '!!!>' ), args.join(' ')));
};
