#!/usr/bin/env node
/**
 * CLI completions verb.  Outputs a bash script that allows tab completion.
 */
'use strict';
const debug = require('debug')('ils:cli:completions');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');
