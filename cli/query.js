#!/usr/bin/env node
/**
 * CLI query verb
 */
'use strict';
const debug = require('debug')('ils:cli:query');
const chalk = require('chalk');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = (program) =>
{
	let command = program
		.command('query <table> [filters...]')
		.alias('q')
		.option('-c, --ilsfile <config file>', 'run with specified ILS file')
		.option('-p, --page <page_number>', 'which page of results to get')
		.option('-l, --limit <number>', 'return no more than this many records')
		.option('-o, --out [output format]','specify output format (table, json or csv)', 'table')
		.option('-f, --fields [fields]')
		.option('-r, --remote', 'prefer to get information directly from Infusionsoft')
		.option('-s, --sort-by [field]', 'specify field to use for sorting', 'Id')
		.option('-d, --sort-desc', 'reverse the sort order')
		.description('display items from Infusionsoft');
	command.action(action(command));
	return command;
};

const action = command => async (table, filters) =>
{
	const text_renderers = require('./text_renderers');
	let errhand = ErrorHandler(() => ils.trigger('close'));

	let ils = ILS(command.ilsfile, true);
	require('./loggers')(ils);

	await ils.ready();

	table = ils.schema[table].canonical_name;

	if (!ils.schema[table].fields)
		throw Error(`Unknown table: ${table}. Valid tables are: ${Object.keys(ils.schema).join(',')}`);

	let fields = command.fields;

	if (fields)
	{
		fields = fields.split(',');
		fields = validize_fields(ils.schema[table].fields, fields);
	}
	else
		fields = ils.schema[table].fields;

	let q = ils.query[table].select(fields);

	if (filters)
		apply_filters(q, filters);

	if (command.sortBy)
	{
		//can't sort by the default value on the ONLY table that doesn't have it.
		if (command.sortBy === 'Id' && table === 'ContactGroupAssign')
			command.sortBy = 'DateCreated';
		q.orderBy(command.sortBy, command.sortDesc ? 'asc' : 'desc');
	}

	if (command.limit)
		q.limit(parseInt(command.limit));
	if (command.page > -1)
		q.page(parseInt(command.page));

	if (command.remote)
		q.remote();

	let renderer = text_renderers.get_renderer(command.out, fields);

	let res = await q.execute().catch(errhand);

	renderer(res);

	if (process.stderr.isTTY)
	{
		let str = chalk.yellow(res.length) + ' records shown';

		if (!q.remoteForced)
			str += `, ${chalk.yellow(await q.local().count())} matches in local DB`;
		if (!q.localForced)
			str += `, ${chalk.yellow(await q.remote().count())} matches in Infusionsoft`;

		str += '.'

		console.error(str);
	}

	if (!await ils.sync.isUpToDate(table))
	{
		console.log();
		ils.trigger('log-warn', 
			table,
			'table is not up to date-- some entries may be missing or have incorrect information.',
			'Run', chalk.yellow('ils sync', table), 'to update.');
	}

	ils.close();
};

/** 
 * returns a correct list of fields, capitalized and clipped.
 */
const validize_fields = (existing_fields, query_fields) =>
{
	let lc_fields = existing_fields.map(f => f.toLowerCase());

	return query_fields.map((f, idx) =>
	{
		if (existing_fields.indexOf(f) < 0)
		{
			if (lc_fields.indexOf(f) < 0)
				throw Error('Unknown field: ' + f);
			else
				f = existing_fields[lc_fields.indexOf(f.toLowerCase())];
		}
		return f;
	});
};

/**
 * applies filters as agreed
 */
const apply_filters = (q, filters) =>
{
	filters.forEach(str =>
	{
		let brk = str.match(/(.*)([=%<>])(.*)/);
		if (!brk)
			throw Error('Invalid query: ' + str);
		let field    = brk[1];
		let operator = brk[2];
		let value    = brk[3];

		if (field === 'tagged')
			return q.tagged(value.split(','));
		
		if (field.toLowerCase() === 'nottagged' && q.tagged)
			return q.notTagged(value.split(','));

		if ((field === 'Id' || field === 'OwnerID') && value.indexOf(',') > 0)
			value = value.split(',').map(s => parseInt(s));

		switch(operator)
		{
			case '=':
				if (Array.isArray(value))
					q.whereIn(field, value);
				else if (value === 'NULL' || value === '')
					q.whereNull(field);
				else
					q.where(field, '=', value);
				break;
			case '%':
				q.where(field, 'LIKE', '%' + value + '%');
				break;
			case '>':
				q.where(field, '>', value);
				break;
			case '<':
				q.where(field, '<', value);
				break;
		}
	});
	return q;
};

