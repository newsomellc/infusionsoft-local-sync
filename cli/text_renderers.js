'use strict';
/**
 * Helpers to render Infusionsoft data.
 */

/**
 * The simplest of all-- just outputs JSON data.
 */
exports.json_out = headers => o =>
{
	if (!headers)
		return console.log(JSON.stringify(o));
	else if (Array.isArray(headers))
		var cols = headers;
	else
	{
		var cols = Object.keys(headers);
		headers = Object.values(headers);
	}

	let process_obj = osubo =>
	{
		let o = Object.create(null);
		cols.forEach((col, i) =>
		{
			o[headers[i]] = osubo[col];
		});
		return o;
	};

	if (Array.isArray(o))
		return console.log(JSON.stringify(o.map(process_obj)));
	else
		return console.log(JSON.stringify(process_obj(o)));
};

/**
 * Outputs a human readable table.
 */
exports.table_out = headers => o =>
{
	let Moment = require('moment');
	//Table output happens using parallel arrays: cols and headers.
	//cols is the object key used to access object keys on o.
	//headers is the human readable top display.
	//in array mode, cols isn't needed.
	const chalk = require('chalk');
	if (!o.length)
		return console.log(chalk.cyan('No results returned.'));

	let mode = o.reduce((homogenous, osubo) => 
	{
		switch (homogenous)
		{
			case 'array':
				if (!Array.isArray(osubo))
					throw Error('can\'t mix arrays and objects in table output.');
				break;
			case 'object':
				if (typeof(osubo) !== 'object')
					throw Error('can\'t mix arrays and objects in table output.');
				break;
			case 'not_checked':
				if (Array.isArray(osubo))
					return 'array';
				else if (typeof(osubo) === 'object')
					return 'object';
				else
					throw Error(`Invalid type passed: ${typeof(osubo)}`);
		}
		return homogenous;
	}, 'not_checked');

	let cutoff, cols;

	switch (mode)
	{
		case 'array':
			if (!headers)
				throw Error('Must specify headers in array mode.');
			cols = headers.map((h, i) => i);
			break;
		case 'object':
			//in object mode, we assume that array headers ARE the keys.
			if (Array.isArray(headers))
				cols = headers;
			else
				cols    = Object.keys(headers);
				headers = Object.values(headers);
			//Elide completely empty columns.
			let elides = cols.map((h, i) => !o.reduce((hit, subo) => hit || subo.hasOwnProperty(cols[i]), false));
			headers = headers.filter((h, i) => !elides[i]);
			cols = cols.filter((h, i) => !elides[i]);
			break;
	}


	let colsizes = headers.map(head => head.length);

	//First pass-- size the table before laying it out.
	o.forEach(osubo =>
	{
		cols.forEach((col, index) =>
		{
			if (osubo[col] === undefined)
				var str = '-';
			else if (osubo[col] === null)
				var str = '-';
			else if (osubo[col] instanceof Date)
				var str = Moment(osubo[col]).format('MMM D YYYY');
			else
				var str = osubo[col].toString();
			colsizes[index] = Math.max(str.length, colsizes[index]);
		});
	});

	{
		cutoff = 0;
		let _cols = [];
		let i = 0;
		let tabwidth = 4;
		while (tabwidth + colsizes[i] < process.stdout.columns)
		{
			_cols.push(cols[i]);
			tabwidth += colsizes[i] + 3;
			i++;
		}
		cutoff = cols.length - _cols.length;
		cols = _cols;
		colsizes.length = _cols.length;
	}

	if (!colsizes.length)
		throw Error('No representable data in the columns requested-- all fields in all rows are empty.');


	let table_size = colsizes.reduce((car,cur) => car + cur) + 4 + (cols.length-1) * 3 ;

	const render_header = () =>
	{
		console.log(chalk.cyan(fit_str('', table_size, '-')));
		{
			let out = '';
			out += chalk.cyan('| ');
			cols.forEach((col, i) =>
			{
				out += fit_str(headers[i], colsizes[i], ' ', 'center');

				if (i !== cols.length-1)
					out += chalk.cyan(' | ');
			});
			out += chalk.cyan(' |');
			console.log(out);
		}
		console.log(chalk.cyan(fit_str('', table_size, '-')));
	}

	render_header();
	o.forEach(osubo =>
	{
		let out = '';
		out += chalk.cyan('| ');
		cols.forEach((col, i) =>
		{
			if (osubo[col] === undefined)
				var str = '-';
			else if (osubo[col] === null)
				var str = '-null-';
			else if (osubo[col] instanceof Date)
				var str = Moment(osubo[col]).format('MMM D YYYY');
			else
				var str = osubo[col].toString();

			if (!osubo[col])
				str = chalk.dim.blue(fit_str(str, colsizes[i], ' ', 'center'));
			else
				str = fit_str(str, colsizes[i], ' ', 'left');

			out += str;

			if (i !== cols.length-1)
				out += chalk.cyan(' | ');
		});
		out += chalk.cyan(' |');

		console.log(out);
	});
	render_header();
	if (cutoff)
		console.log(chalk.bold.cyan('Note: ' + cutoff + ' columns not displayed due to available screen space. Use'), chalk.bold.yellow('-f'), chalk.bold.cyan('parameter to select desired columns.'));

};

/**
 * Outputs the data as a CSV file.
 */
exports.csv_out = headers => o =>
{
};

/**
 * Convenience method.  Gets the renderer from args.
 */
exports.get_renderer = (output, fields) =>
{
	switch (output && output.toUpperCase())
	{
		case 'JSON' :
			return exports.json_out(fields);
		case 'CSV' :
			return exports.csv_out(fields);
		default:
		case 'TEXT' :
			return exports.table_out(fields);
	}
}

/**
 * Pad or crop the given string to the given length with the given char.
 */
const fit_str = (str, length, char=' ', mode='left') =>
{
	if (str.length > length)
		return str.substr(0, length-3) + '...';
	for (let i = str.length; i < length; i++)
		switch (mode)
		{
			case 'left':
				str = str+char;
				break;
			case 'right':
				str = char+str;
				break;
			case 'center':
				str = i % 2 ? str+char : char+str;
				break;
		}
	return str;
}
