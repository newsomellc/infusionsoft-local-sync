/**
 * CLI setup verb.  Starts a wizard process that genereates a json configuration
 file, named `infusionsoft-local-sync.json`.
 */
'use strict';
const debug       = require('debug')('ils:cli:setup');
const file_exists = require('fs').existsSync;
const chalk       = require('chalk');
const path        = require('path');
const NConf       = require('nconf').Provider;

const ILS   = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = program =>
{
	program
		.command('setup')
		.option('-c, --ilsfile <config file>', 'create a new config based on the given ILS file')
		.option('-d, --directory <filename>', 'directory for output config file', process.cwd())
		.option('-e, --environment <envname>', 'generate config for given environment (basically just places ".<envname>" before ".json" in the output file\'s name)')
		.option('-f, --file-name <filename>', 'set full file name for output file. Ignores all other options that influence the filename.')
		.option('-u, --user', 'generate config for current user')
		.option('-g, --global', 'generate systemwide config (may need superuser/admin credentials)')
		.description('set up ILS to connect with your Infusionsoft account.')
		.action(action(program));
};

const action = program => () => 
{
	let respath;
	let config = new NConf();

	let ask_question = get_question_asker();

	config.file(path.join(__dirname, '..', 'res', 'defaults.json'));

	/** 
	 * Runs through the setup process, asking questions, setting up config.
	 */
	const setup_process = step =>
	{	
		debug('setup process step: ', step);

		let next = () =>
		{
			if (step !== 'done')
				return setup_process(next_step(step));
		};

		switch (step)
		{
			default:
			case 'api':
				write_heading('Section 1: API credentials.');
				step = next_step(step);
				// API config section.
			case 'api:app':
				return ask_question('Your Infusionsoft app name (e.g. ab123)', config.get(step), 'string').then(set_val(step)).then(next);
			case 'api:client_id':
				return ask_question('Your Infusionsoft Application client ID', config.get(step), 'string').then(set_val(step)).then(next);

			case 'api:client_secret':
				return ask_question('Your Infusionsoft Application client secret', config.get(step), 'string').then(set_val(step)).then(next);

			case 'api:retry_count':
				//just sets the default of 5.
				set_val(step)(config.get('api:retry_count')||5);
				step = next_step(step)
			case 'db':
				write_heading('Section 2: Database setup.');
				console.log(chalk.cyan('ILS can synchronize Infusionsoft with a local PostgreSQL database.'));
				console.log(chalk.cyan('In addition to providing you with a local copy of your Infusionsoft records, this also allows you to run advanced queries, and use the database (read-only) in your own applications.'));
				return ask_question('Do you want to configure ILS\'s database connection?', !!config.get('web'), 'boolean').then(result =>
				{
					if (!result)
						return setup_process('web');
					else
						return setup_process(next_step(step));
				});


			case 'db:client':
				//just assume the default ('pg') because I haven't coded support for anything else yet, though it's theoretically workable.
				set_val(step)(config.get('db:client')||'pg');
				step = next_step(step);
			case 'db:connection':
				let dbconf = config.get(step);
				let db_setup = () =>
				        ask_question('database host',     config.get('db:connection:host') || '',        'string').then(set_val('db:connection:host')).then(() =>
					    ask_question('database port',     config.get('db:connection:port') || undefined, 'integer').then(set_val('db:connection:port'))).then(() =>
					    ask_question('database username', config.get('db:connection:user') || '',        'string').then(set_val('db:connection:user'))).then(() =>
					    ask_question('database password', config.get('db:connection:password') || '',    'string').then(set_val('db:connection:password'))).then(() =>
					    ask_question('database name',     config.get('db:connection:database') || 'ils', 'string').then(set_val('db:connection:database')));

				if (typeof dbconf === 'object')
					return db_setup();
				else 
					return ask_question('Database connection string (just press enter if your connection uses user/password/host etc)', config.get(step), 'string')
					.then(result => 
					{
						if (result)
						{
							return set_val('db:connection', result);
						}

						return db_setup();

					});
			case 'db:searchPath':

				return ask_question('PostgreSQL schema', config.get('db:searchPath') || config.get('api:app'), 'string')
					.then(result => set_val(step)([result]));


			case 'web':
				write_heading('Section 3: Web host setup.');
				console.log(chalk.cyan('Setting up ILS with a globally accessible web endpoint is a great lightweight way to get instant object updates from Infusionsoft and authenticate users for custom services.'));
				console.log(chalk.cyan('This will only work if the system you\'re running ILS on is globally accessible, and you can configure your webserver to forward requests to it.'));
				console.log(chalk.cyan('See the setup guide for more information.'));
				return ask_question('Do you want to configure ILS\'s web endpoint?', !!config.get('web'), 'boolean').then(result =>
				{
					if (!result)
						setup_process('login');
					else
						setup_process(next_step(step));
				});
			case 'web:local_host':
			case 'web:local_path':
			case 'web:use_https':
			case 'web:listen':
			case 'web:use_passport':
			case 'login':
				console.log(chalk.red('login process not set up yet.'));
			case 'done':
				//write config, inform user.
		}
	};

	/** 
	 * Sets value in config, advances the step.
	 */
	const set_val = key => value =>
	{
		config.set(key, value);
		return Promise.resolve();
	};


	setup_process(SETUP_STEPS[0]);


};

/**
 * Asks a question, returns a promise that resolves to user input.
 */
const get_question_asker = () => 
{
	let stdin = process.openStdin();

	return (question, default_value, type) =>
	{

		let wr = (...o) => process.stdout.write(...o);
		wr(chalk.green(question));
		if (default_value !== undefined && default_value !== null)
		{
			wr(chalk.green(' ['));
			switch (type)
			{
				case 'boolean':
					wr(chalk.bold(default_value ? 'y' : 'n'));
					break;
				default:
					wr(chalk.bold(default_value));
					break;
			}
			wr(chalk.green(']'));
		}
		wr(chalk.green(': '));

		return new Promise((res, rej) =>
		{
			let on_entry = data => 
			{
				debug('processing ' + data.toString());

				if (data)
				{
					data = data.toString().trim();
					debug('returning ' + data);
					switch (type)
					{
						case 'boolean':
							res(['y', 't', 'yes'].indexOf(data.toLowerCase())>-1);
							break;
						default : 
							res(data);
							break;
					}
					stdin.removeListener('data', on_entry)
				}
			};
			stdin.addListener('data', on_entry);
		});
	};
};

/** 
 * Writes a pretty-ish heading.
 */ 
const write_heading = string => 
{
	let wr      = (...o) => process.stdout.write(...o);
	let cols    = process.stdout.columns;
	let line    = new Array(cols).join('-');
	let padding = new Array(Math.max(Math.ceil((process.stdout.columns - string.length - 4) / 2), 0)).join(' ');

	wr(chalk.green(line));
	wr('\n');

	if (string.length % 2)
		wr(chalk.green(' '));
	wr(chalk.green(padding));
	wr(chalk.green('  '));
	wr(chalk.bold.green(string));
	

	wr(chalk.green('  '));
	wr(chalk.green(padding));
	wr('\n');


	wr(chalk.green(line));
	wr('\n');

};

/** 
 * A list of keys our config needs to run through.
 */
const SETUP_STEPS =
[
	'api',
	'api:app',
	'api:client_id',
	'api:client_secret',
	'api:retry_count',
	'web',
	'web:local_host',
	'web:local_path',
	'web:use_https',
	'web:listen',
	'web:use_passport',
	'db',
	'db:client',
	'db:database',
	'db:connection',
	'db:searchPath',
	'login',
	'done',
];
/** 
 * Gets the next step given the current.
 */
let next_step = current => 
{
	let idx = SETUP_STEPS.indexOf(current);
	if (idx+1 >= SETUP_STEPS.length)
		return 'done';
	else
		return SETUP_STEPS[idx+1];
}

