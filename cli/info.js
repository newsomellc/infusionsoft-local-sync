#!/usr/bin/env node
/**
 * CLI info verb.  Gets misc info about Infusionsoft, might be helpful for bash completions.
 * Can include such non-table-based metadata as field lists, opportunity stages, etc.
 */
'use strict';
const debug = require('debug')('ils:cli:info');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');
