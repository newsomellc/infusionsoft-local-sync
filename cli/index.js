#!/usr/bin/env node
/**
 * Infusionsoft local sync CLI interface.
 */
'use strict';
const chalk = require('chalk');
const path  = require('path');
const fs    = require('fs');
const Command = require('commander').Command;

let program = new Command('ils');

let splash = () =>
{
	let r =  chalk.reset();
	console.log();
	console.log(chalk.bold.blue(fs.readFileSync(path.join(__dirname, '../res/logo.txt'))), r);
	console.log();
	console.log(chalk.bold.cyan('ils - Infusionsoft Local Sync, the missing command-line (and Node.js) API for Infusionsoft'));
	program.outputHelp();
};


program
	.version('0.0.1');

//require('./completions')(program);
require('./db')(program);
//require('./help')(program);
//require('./ils')(program);
//require('./info')(program);
require('./whoami')(program);
require('./query')(program);
require('./sync')(program);
require('./setup')(program);

program.parse(process.argv);
if (!process.argv.slice(2).length)
	splash();
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});