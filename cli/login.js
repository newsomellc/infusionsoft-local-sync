#!/usr/bin/env node
/**
 * CLI setup verb.  Starts a wizard process that genereates a json configuration
 file, named `infusionsoft-local-sync.json`.
 */
'use strict';
const debug = require('debug')('ils:cli:query');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = program =>
{
		.command('query <table> [filters...]')
		.alias('q')
		.option('-c, --ilsfile <config file>', 'create a new config based on given ILS file')
		.description('set up ILS to connect with your app.');

};

const action = () => 
{
	
};
