'use strict';
const debug = require('debug')('ils:cli:whoami');

const ILS          = require('../src');
const ErrorHandler = require('./error-handler');

module.exports = (program) =>
{
	let command = program
		.command('whoami')
		.alias('who')
		.option('-c, --ilsfile <config file>', 'Run with specified ILS file')
		.option('-o, --out [output format]','specify output format (table, json or csv)', 'table')
		.option('-v, --verbose', 'get more user info')
		.description('get info about the logged-in user');
	command.action(action(command));
	return command;
};

const action = command => () =>
{
	const ILS = require('../src');
	let ils = ILS(command.ilsfile);
	ils.q.getCurrentUser().then(user_info =>
	{
		let renderer = text_renderers.get_renderer(command.out, fields);
		ils.close();
	});
};
